<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Otosales-NewOrder</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/AO/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/MGO/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELERENEWAL/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL ,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK,LIM</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/TELESALES/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
