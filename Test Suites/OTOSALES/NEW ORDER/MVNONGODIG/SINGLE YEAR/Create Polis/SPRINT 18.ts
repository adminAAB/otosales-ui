<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>SPRINT 18</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY - AO - UMA - edit</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - AO - UMA -Edit</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - TELERENEWAL - Edit</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - TELERENEWAL - Edit - Copy</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - AO - UMA -Edit - Copy</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - TELERENEWAL - Edit - Copy</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY - AO - UMA - edit - Copy</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - TELERENEWAL - Edit</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
