<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Tasklist otosales</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b5f473a0-7620-4c22-ad65-320662c1bd50</testSuiteGuid>
   <testCaseLink>
      <guid>94bd1931-a6ef-461a-9b2c-7d6baef52383</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ - AO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac5a425a-0048-4be4-9906-2450a665efaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>217a87a5-d580-4312-8532-4da7f0aa802b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ - MGO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d30ff74f-dad1-46da-a776-99d684ca02ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ - MGO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>202c7917-8c52-4bed-baf3-7f7d8eb6c875</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - MGO - Cabang</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dda31e87-ccae-4e4e-aeb1-957c68694c0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - AO - UMA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e25d5b39-98aa-4c63-b775-07aa0e5bf21a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ - MGO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abe7633d-beb7-4cb6-9bd6-311541678869</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51d640ab-ac7e-450f-8338-6ce85c43dfbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY - AO - UMA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>374a2112-0022-4152-9a8a-bfcb53b75c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY,NOAPPADJ - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73c063e3-3d78-40f1-9f5c-85c8ba0d0a02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL - TELESALES</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2af04d84-a5f4-4b70-8ead-c79a68f31ffe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ - AO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f68959c0-8998-441a-ba36-01a94a193927</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ - MGO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ed4388d-ced3-48f9-8157-5e6b3530c0c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,COMPANY - AO - UMA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52372706-e8e7-435b-9ddb-e7b8918e35cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY - AO - UMA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>865ec9c3-3215-4766-894e-8744811cfd7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c58c293-a1fe-4968-b31b-cef0e8e5de47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ - AO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aca239c4-2cce-4bf7-9d16-5f5156ffc177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19cc54ed-13e7-4b97-959f-2fed5eca3a66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL - AO - UMA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1864a442-bc2d-4dc9-928e-4a8d862eda88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL - TELERENEWAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1f12d0d-4306-4cc2-954b-2c3babcf2760</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/SINGLE YEAR/Create Polis/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ - AO</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
