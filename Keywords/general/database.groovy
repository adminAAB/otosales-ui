package general

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.util.ArrayList
import java.sql.PreparedStatement
import java.sql.Driver
import java.sql.Connection
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class database {
	private static Connection connection = null;

	@Keyword

	def connectLiTT(){
		(new general.database().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))
	}

	@Keyword

	def connectAAB(){
		(new general.database().connectDB('172.16.94.74', 'AAB', 'ITAPP', 'It5Fu#H0m317'))
	}

	@Keyword

	private def connectDB(String url, String dbname, String username, String password){

		String conn = "jdbc:sqlserver://" + url + ";databasename=" + dbname

		if(connection != null && !connection.isClosed()){

			connection.close()
		}

		connection = DriverManager.getConnection(conn, username, password)

		return connection
	}

	@Keyword

	def executeQuery(String queryString) {

		Statement stm = connection.createStatement()

		ResultSet rs = stm.executeQuery(queryString)

		return rs
	}

	@Keyword

	def execute(String queryString) {

		Statement stm = connection.createStatement()

		boolean result = stm.execute(queryString)

		return result
	}

	@Keyword

	def closeDatabaseConnection() {

		if(connection != null && !connection.isClosed()){

			connection.close()
		}

		connection = null
	}
}
