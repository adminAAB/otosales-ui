package general

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class scheduler extends com.keyword.UI {

	@Keyword
	def Otosales_CreateOrder(){
		String scheduler = '\\\\172.16.94.33\\c$\\Program Files (x86)\\Asuransi Astra\\Otosales\\Otosales.CreateOrder\\Otosales.CreateOrder.exe'

		if (verifyStaging()) {
			scheduler = '\\\\172.16.93.33\\d$\\Otosales\\Otosales.CreateOrder\\Otosales.CreateOrder.exe'
		}

		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println (scheduler)
			println('Scheduler CreateOrder Still Running')
			WebUI.delay(5)
		}
		println('Scheduler CreateOrder is done')
	}

	@Keyword
	def Otosales_MonitorOrder(){
		String scheduler = '\\\\172.16.94.33\\c$\\Program Files (x86)\\Asuransi Astra\\Otosales\\Otosales.MonitorOrder\\Otosales.MonitorOrder.exe'

		if (verifyStaging()) {
			scheduler = '\\\\172.16.93.33\\d$\\Otosales\\Otosales.MonitorOrder\\Otosales.MonitorOrder.exe'
		}

		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println (scheduler)
			println('Scheduler MonitorOrder Still Running')
			WebUI.delay(5)
		}
		println('Scheduler MonitorOrder is done')
	}

	@Keyword
	def Otosales_Approval_Renew(){
		String scheduler = '\\\\172.16.94.97\\c$\\Otosales\\Otosales.ApprovalRenew\\Otosales.Approval.exe'
		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println('Scheduler ApprovalRenew Still Running')
			WebUI.delay(5)
		}
		println('Scheduler ApprovalRenew is done')
	}

	@Keyword
	def Otosales_Approval_New(){
		String scheduler = '\\\\172.16.94.97\\c$\\Otosales\\Otosales.ApprovalNew\\Otosales.Approval.exe'

		if (verifyStaging()) {
			scheduler = '\\\\172.16.93.33\\d$\\Otosales\\Otosales.ApprovalNew\\Otosales.Approval.exe'
		}

		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println (scheduler)
			println('Scheduler ApprovalNew Still Running')
			WebUI.delay(5)
		}
		println('Scheduler ApprovalNew is done')
	}

	@Keyword
	def Otosales_Approval_Next_Limit(){
		String scheduler = '\\\\172.16.94.97\\c$\\Otosales\\Otosales.ApprovalNextLimit\\Otosales.Approval.exe'

		if (verifyStaging()) {
			scheduler = '\\\\172.16.93.33\\d$\\Otosales\\Otosales.ApprovalNextLimit\\Otosales.Approval.exe'
		}

		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println (scheduler)
			println('Scheduler ApprovalNextLimit Still Running')
			WebUI.delay(5)
		}
		println('Scheduler ApprovalNextLimit is done')
	}

	@Keyword
	def Otosales_Approval_Gen5(){
		String scheduler = '\\\\172.16.94.97\\c$\\Otosales\\Otosales.ApprovalGEN5\\Otosales.Approval.exe'

		if (verifyStaging()) {
			scheduler = '\\\\172.16.93.33\\d$\\Otosales\\Otosales.ApprovalGEN5\\Otosales.Approval.exe'
		}

		Process s = Runtime.getRuntime().exec(scheduler)
		while (s.isAlive()){
			println (scheduler)
			println('Scheduler ApprovalGen5 Still Running')
			WebUI.delay(5)
		}
		println('Scheduler ApprovalGen5 is done')
	}
}
