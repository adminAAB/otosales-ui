package general

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import java.lang.Object
import java.lang.ProcessBuilder

import java.time.Duration
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import io.appium.java_client.MultiTouchAction as MultiTouchAction
import io.appium.java_client.TouchAction as TouchAction
import org.openqa.selenium.By

import com.kms.katalon.core.webui.driver.DriverFactory

public class mobile {

	@Keyword

	def getDeviceInfo(String currentPlatform, String param){
		String[] splitPlatform = currentPlatform.split("\\.")
		switch (splitPlatform[0]) {
			case '7':
			case '8':
			case '9':
				if(param=="ID")
					return ["id", "ID"]
				else if(param=="EN")
					return ["en", "EN"]
				break
			case '5':
				if(param=="ID")
					return ["in", "ID"]
				else if(param=="EN")
					return ["en", "US"]
				break
			default:
				if(param=="ID")
					return ["in", "ID"]
				else if(param=="EN")
					return ["en", "US"]
				break
		}
	}

	@Keyword

	def takeScreenshot(String filename) {
		//development process
		if(!GlobalVariable.ScreenshotSet){
			GlobalVariable.ScreenshotSet = true
			String dateToday = new general.master().getDateToday('dd-MMMM-HH-mm')
			String[] splitDate = dateToday.split("-")
			GlobalVariable.ScreenshotFolder = splitDate[0] + ' ' + splitDate[1]
			GlobalVariable.ScreenshotTime = splitDate[2] + '-' + splitDate[3] + '_'
		}
		String fullpath = GlobalVariable.ScreenshotPath + '/' + GlobalVariable.ScreenshotPhone + '/' + GlobalVariable.ScreenshotFolder + '/' + GlobalVariable.ScreenshotTime + GlobalVariable.ScreenshotTS + '/' +  GlobalVariable.ScreenshotUrutan +'. ' + filename
		GlobalVariable.ScreenshotUrutan = GlobalVariable.ScreenshotUrutan + 1

		Mobile.takeScreenshot(fullpath)
	}

	@Keyword

	def takeWebScreenshot(String filename) {
		String fullpath = GlobalVariable.ScreenshotPath + '/' + GlobalVariable.ScreenshotPhone + '/' + GlobalVariable.ScreenshotFolder + '/' + GlobalVariable.ScreenshotTS + '/' +  GlobalVariable.ScreenshotUrutan +'. ' + filename

		GlobalVariable.ScreenshotUrutan = GlobalVariable.ScreenshotUrutan + 1

		WebUI.takeScreenshot(fullpath)
	}

	@Keyword

	def clickPortion(float x, float y) {
		//nilai maks 9.999
		//x mendatar, y menurun
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		Mobile.tapAtPosition(maxWidth * x / 10, maxHeight * y / 10)
	}

	@Keyword

	def swipePortion(float startX, float startY, float endX, float endY) {
		//nilai maks 9.999
		//x mendatar, y menurun
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		int x1 = Math.round(maxWidth * startX / 10)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth * endX / 10)

		int y2 = Math.round(maxHeight * endY / 10)

		Mobile.swipe(x1, y1, x2, y2)
	}

	@Keyword

	def clickPivot(TestObject objectWanted, String pivot, int pixelX, int pixelY) {
		//cari sebuah elemen, pivot elemen tersebut pada bagian atas kanan kiri atau bawah. Dari elemen tersebut klik pada pixel x dan y
		//x mendatar kanan, y menurun berupa pixel. y negatif keatas, x negatif kekiri
		int x = 0, y = 0
		switch (pivot.toUpperCase()) {
			case 'ATAS':
				x = 5
				break
			case 'BAWAH':
				x = 5
				y = 10
				break
			case 'KIRI':
				y = 5
				break
			case 'KANAN':
				x = 10
				y = 5
				break
		}

		int elPosX = Mobile.getElementLeftPosition(objectWanted, 10)

		int elPosY = Mobile.getElementTopPosition(objectWanted, 10)

		int elHeight = Mobile.getElementHeight(objectWanted, 10)

		int elWidth = Mobile.getElementWidth(objectWanted, 10)

		int finalX = elPosX + (elWidth * x)/10 + pixelX

		int finalY = elPosY + (elHeight * y)/10 + pixelY

		Mobile.tapAtPosition(finalX, finalY)
	}

	@Keyword

	def scrollSearch(TestObject objectWanted,  float startX, float startY, float endX, float endY, int maxScroll) {
		//nilai maks 9.999 untuk startXY dan endXY
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		int x1 = Math.round(maxWidth * startX / 10)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth * endX / 10)

		int y2 = Math.round(maxHeight * endY / 10)

		while(maxScroll-->0){
			int loc = maxHeight
			try{
				loc = Mobile.getElementTopPosition(objectWanted, 2)
			}
			catch(Exception ex){

			}
			if(loc >= maxHeight) {
				//Mobile.swipe(x1, y1, x2, y2)
				TouchAction swipe = new TouchAction(AppiumDriverManager.driver).press(x1, y1).waitAction(Duration.ofSeconds(1)).moveTo(x2, y2).release()
				swipe.perform();
				Mobile.delay(2)
			}
		}
	}

	@Keyword

	def scrollSearchAvoidBottomPanel(TestObject objectWanted,  float startX, float startY, float endX, float endY, int maxScroll, String panelName) {
		//nilai maks 9.99 untuk startXY dan endXY. Nilai 10 sudah menyentuh panel yang ingin dihindari (1-10 proporsi diluar panel)
		//Panel position: Bottom (Karena asumsi start dimulai dari top page, dan pengaturan jarak scrollmu tidak membuat elemen tertutup pada top panel)
		float maxWidth = (float) Mobile.getDeviceWidth()

		float deviceHeight = (float) Mobile.getDeviceHeight()

		int maxHeight = 0

		switch (panelName.toUpperCase()) {
			case 'HOME':
				maxHeight = Mobile.getElementTopPosition(findTestObject('Object Repository/Mobile/Otocare-Home/Panel-BottomContainer'), 0)
				break
			case 'GM':
				maxHeight = Mobile.getElementTopPosition(findTestObject('Object Repository/Mobile/Otocare-GardaMall/QuickAccess/Panel-BottomContainer'), 0)
				break
		}

		int x1 = Math.round(maxWidth * startX / 10)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth * endX / 10)

		int y2 = Math.round(maxHeight * endY / 10)

		while(maxScroll-->0){
			//mungkin butuh try catch kalo error
			int loc = deviceHeight
			try{
				loc = Mobile.getElementTopPosition(objectWanted, 2)
			}
			catch(Exception ex){

			}
			if(loc >= maxHeight) Mobile.swipe(x1, y1, x2, y2)
		}
	}

	@Keyword

	def scrollDownScreenShotUntil(TestObject objectWanted, String filename, float startY, float endY, int maxScroll) {
		//nilai maks 9.999 untuk startXY dan endXY
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		int x1 = Math.round(maxWidth / 2)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth / 2)

		int y2 = Math.round(maxHeight * endY / 10)

		int progress = 1

		while(maxScroll-->0){
			int loc = maxHeight
			try{
				takeScreenshot('_' + progress + '_' + filename)
				progress++
				loc = Mobile.getElementTopPosition(objectWanted, 2)
			}
			catch(Exception ex){

			}
			if(loc >= maxHeight)
			{
				TouchAction swipe = new TouchAction(AppiumDriverManager.driver).press(x1, y1).waitAction(Duration.ofSeconds(1)).moveTo(x2, y2).release()
				swipe.perform();
				Mobile.delay(2)
			}
			else break
		}
	}

	@Keyword

	def scrollMultiple(float startX, float startY, float endX, float endY, int maxScroll) {
		//nilai maks 9.99 untuk startXY dan endXY. Nilai 10 sudah menyentuh panel yang ingin dihindari (1-10 proporsi diluar panel)
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		int x1 = Math.round(maxWidth * startX / 10)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth * endX / 10)

		int y2 = Math.round(maxHeight * endY / 10)

		while(maxScroll-->0){
			//Mobile.swipe(x1, y1, x2, y2)
			TouchAction swipe = new TouchAction(AppiumDriverManager.driver).press(x1, y1).waitAction(Duration.ofSeconds(1)).moveTo(x2, y2).release()
			swipe.perform();
			Mobile.delay(2)
		}
	}

	@Keyword

	def waitDisappear(TestObject objectWanted,  int timeSeconds) {
		while(timeSeconds>0){
			Boolean status = true
			try{
				status = Mobile.waitForElementPresent(objectWanted, 2)
			}
			catch(Exception ex){

			}
			if(status) timeSeconds-=2
			else
				return true
		}
		return false
	}

	@Keyword

	def writeShell(TestObject objectWanted, String inputText) {
		Mobile.tap(objectWanted, 0)

		Mobile.delay(2)

		String deviceUUID = AppiumDriverManager.getDeviceId(DriverFactory.MOBILE_DRIVER_PROPERTY)
		try {
			ProcessBuilder pb2 = new ProcessBuilder("adb", "-s", deviceUUID, "shell", "input", "text", inputText).redirectErrorStream(true).start();
		} catch (Exception exx) {
			exx.printStackTrace()
		}
	}

	@Keyword

	def clickLastChild(TestObject objectWanted) {
		//some element has list of child that you may want to click just the latest element which you don't know the count
		AppiumDriver driver = MobileDriverFactory.getDriver()

		List childElements = driver.findElements(By.xpath(objectWanted.findPropertyValue('xpath') + '/*'))

		int totalElement = childElements.size()

		childElements[totalElement-1].click()
	}

	@Keyword

	def selectListItemByLabelContains(TestObject objectWanted, String text) {
		//some element has list of child that you may want to know the index using CONTAINS.
		AppiumDriver driver = MobileDriverFactory.getDriver()

		List childElements = driver.findElements(By.xpath(objectWanted.findPropertyValue('xpath') + '/*'))

		for(int i=0;i<childElements.size();i++){
			String MobText = childElements[i].getText()

			if(MobText.contains(text)){
				childElements[i].click()

				break;
			}
			if(i==childElements.size()-1)
				childElements[i].click()
		}
	}

	@Keyword

	def selectListItemByLabelandScroll(TestObject objectWanted, String text, float startX, float startY, float endX, float endY, int maxScroll) {
		float maxWidth = (float) Mobile.getDeviceWidth()

		float maxHeight = (float) Mobile.getDeviceHeight()

		int x1 = Math.round(maxWidth * startX / 10)

		int y1 = Math.round(maxHeight * startY / 10)

		int x2 = Math.round(maxWidth * endX / 10)

		int y2 = Math.round(maxHeight * endY / 10)

		while(maxScroll-->0){
			try{
				Mobile.selectListItemByLabel(objectWanted, text, 5)
				break
			}
			catch (Exception ex){
				TouchAction swipe = new TouchAction(AppiumDriverManager.driver).press(x1, y1).waitAction(Duration.ofSeconds(1)).moveTo(x2, y2).release()
				swipe.perform();
				Mobile.delay(2)
			}
		}
	}

	@Keyword

	def writeLetterByLetter(TestObject objectWanted,  String input) {
		//sitok2
		for (def index : (0..input.length() - 1)) {
			Mobile.sendKeys(objectWanted, input.substring(index, index+1))
		}
	}

	@Keyword

	def datePicker(String date) {
		//2020-02-05
		//Function not complete, now it just ignore month and day
		String Year = date.substring(0, 4)

		String Month = date.substring(5, 7)

		String Day = date.substring(8, 10)

		Mobile.waitForElementPresent(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_year'), 10)

		Mobile.tap(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_year'), 0)

		Mobile.waitForElementPresent(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_lst_year'), 10)

		Mobile.selectListItemByLabel(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_lst_year'), Year, 10)

		Mobile.waitForElementPresent(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_OK'), 10)

		Mobile.tap(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_date_5'), 0)

		Mobile.tap(findTestObject('OTOSURVEY/CLAIM_SURVEY/pop_calender_OK'), 0)
	}

	@Keyword

	def captureToast(int timeout) {
		AppiumDriver driverLocal = MobileDriverFactory.getDriver()

		String text=''

		for(int i=0;i<timeout;i++){
			try{
				def toastView = driverLocal.findElement(By.xpath('//android.widget.Toast[1]'))
				text = toastView.getAttribute('name')
				if(text!='')
					break
				else
					Mobile.delay(1)
			}
			catch (Exception ex){
				Mobile.delay(1)
			}
		}

		return text
	}

	@Keyword

	def waitTextAppear(TestObject objectWanted, int timeout) {
		String text
		for(int i=0;i<timeout;i++){
			try{
				text = Mobile.getText(objectWanted, 2)
				if(text!='')
					break
				else
					Mobile.delay(1)
			}
			catch (Exception ex){
				Mobile.delay(1)
			}
		}
		return text
	}

	@Keyword

	def toggleWifi() {
		AppiumDriver driver = MobileDriverFactory.getDriver()

		driver.toggleWifi();
	}

	@Keyword

	def takePhotobyDevice() {
		String phoneType = GlobalVariable.CurrentDevice
		String[] splitPhoneType = phoneType.split(' ')
		if (splitPhoneType[0].toUpperCase() == 'ASUS') {
			Mobile.tap(findTestObject('DEVICE/btn_imgCaptureAsus'), 0)

			Mobile.waitForElementPresent(findTestObject('DEVICE/btn_imgCaptureDoneAsus'), 60)

			Mobile.tap(findTestObject('DEVICE/btn_imgCaptureDoneAsus'), 0)
		}

		else if (splitPhoneType[0].toUpperCase() == 'XIAOMI') {
			Mobile.tap(findTestObject('DEVICE/btn_imgCaptureXiaomi'), 0)

			Mobile.waitForElementPresent(findTestObject('DEVICE/btn_imgCaptureDoneXiaomi'), 60)

			Mobile.tap(findTestObject('DEVICE/btn_imgCaptureDoneXiaomi'), 0)
		}

		else if (splitPhoneType[0].toUpperCase() == 'SAMSUNG') {
			Mobile.waitForElementPresent(findTestObject('DEVICE/btn_cap_Camera'), 60)

			Mobile.delay(2)

			Mobile.tap(findTestObject('DEVICE/btn_cap_Camera'), 0)

			Mobile.waitForElementPresent(findTestObject('DEVICE/btn_Cap_Camera_Ok'), 60)

			Mobile.tap(findTestObject('DEVICE/btn_Cap_Camera_Ok'), 0)
		}
		else {
			Mobile.tap(findTestObject('DEVICE/btn_imgCapture'), 0)

			Mobile.waitForElementPresent(findTestObject('DEVICE/btn_imgCaptureDone'), 60)

			Mobile.tap(findTestObject('DEVICE/btn_imgCaptureDone'), 0)
		}
	}
}
