package general

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By
import org.openqa.selenium.remote.RemoteWebElement

import java.lang.Object
import java.lang.Process
import java.io.File

public class web {

	@Keyword

	def Boolean waitProcessingCommand(){
		int intTimeout = GlobalVariable.LongestDelay
		WebUI.switchToFrame(findTestObject('Object Repository/A2IS-RETAIL/A. frame'), 5)
		WebDriver myDriver = DriverFactory.getWebDriver()
		try {
			while(true){
				//Watchout for stale element error
				//for multiple processing command > "display: none" only appear once in last
				String result = myDriver.findElement(By.xpath("//*[contains(@id,'Blocker_Box')]")).getAttribute("style")
				if(result.contains('display: none;'))
					break
				else
					WebUI.println('Processing command still exist')
				WebUI.delay(1)
				if(intTimeout-- < 0 ){
					throw new Exception("Freeze Processing Command")
				}
			}
		}
		catch(Exception ex)
		{
			WebUI.println('Error Occurred')
			WebUI.println(ex)
		}
		finally
		{
			WebUI.switchToDefaultContent()
			return true
		}
	}

	@Keyword
	def uploadImage1(String fileLocation, String pictureName ){

		def locationFile = GlobalVariable.uploadFileLoc
		def write = new File(locationFile)
		def Location = fileLocation
		write.append(Location)

		def fileName = GlobalVariable.uploadFileName
		def write2 = new File(fileName)
		def name = pictureName
		write2.append(name)

		String upload = GlobalVariable.uploadFileDestination
		Process runUpload = Runtime.getRuntime().exec(upload)

		WebUI.delay(15)

		write.delete()
		write2.delete()
	}

	@Keyword
	def addRegistration(String Region, String Registration){
		if (Region == 'Wilayah 1') {

			def Plate = 'BM' + Registration + 'ATM'
			println (Plate)

			return Plate
		} else if (Region == 'Wilayah 2') {

			def Plate = 'B' + Registration + 'ATM'
			println (Plate)

			return Plate
		} else {

			def Plate = 'AB' + Registration + 'ATM'
			println (Plate)

			return Plate
		}
	}

	@Keyword
	def addChassis(String Region, String Chassis){
		if (Region == 'Wilayah 1') {

			def Rangka = 'BM' + Chassis + 'ATM'
			println (Rangka)

			return Rangka
		} else if (Region == 'Wilayah 2') {

			def Rangka = 'B' + Chassis + 'ATM'
			println (Rangka)

			return Rangka
		} else {

			def Rangka = 'AB' + Chassis + 'ATM'
			println (Rangka)

			return Rangka
		}
	}

	@Keyword
	def addEngine(String Region, String Engine){
		if (Region == 'Wilayah 1') {

			def Mesin = 'BM' + Engine + 'ATM'
			println (Mesin)

			return Mesin
		} else if (Region == 'Wilayah 2') {

			def Mesin = 'B' + Engine + 'ATM'
			println (Mesin)

			return Mesin
		} else {

			def Mesin = 'AB' + Engine + 'ATM'
			println (Mesin)

			return Mesin
		}
	}

	@Keyword
	def getInteger(String Input) {
		String remove = Input.replace(',', '')
		int ParseInt = remove.toInteger()

		return ParseInt
	}

	@Keyword
	def addRegistrationGodig(String Region, String Registration){
		if (Region == 'Wilayah 1') {

			def Plate = 'bm' + Registration + 'atm'
			println (Plate)

			return Plate
		} else if (Region == 'Wilayah 2') {

			def Plate = 'b' + Registration + 'atm'
			println (Plate)

			return Plate
		} else {

			def Plate = 'ab' + Registration + 'atm'
			println (Plate)

			return Plate
		}
	}

	@Keyword
	def addChassisGodig(String Region, String Chassis){
		if (Region == 'Wilayah 1') {

			def Rangka = 'bm' + Chassis + 'atm'
			println (Rangka)

			return Rangka
		} else if (Region == 'Wilayah 2') {

			def Rangka = 'b' + Chassis + 'atm'
			println (Rangka)

			return Rangka
		} else {

			def Rangka = 'ab' + Chassis + 'atm'
			println (Rangka)

			return Rangka
		}
	}

	@Keyword
	def addEngineGodig(String Region, String Engine){
		if (Region == 'Wilayah 1') {

			def Mesin = 'bm' + Engine + 'atm'
			println (Mesin)

			return Mesin
		} else if (Region == 'Wilayah 2') {

			def Mesin = 'b' + Engine + 'atm'
			println (Mesin)

			return Mesin
		} else {

			def Mesin = 'ab' + Engine + 'atm'
			println (Mesin)

			return Mesin
		}
	}

	def reactDatePicker(String newDate) {
		String[] separate = newDate.split('/')

		String Day = separate[0]
		String Month = separate[1]
		String Year = separate[2]

		String getMonth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Datepicker current month'), FailureHandling.STOP_ON_FAILURE)

		int parseInt = Month.toInteger()

		String[] Monthlist = [
			'Unknown',
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		]
		String Selected = Monthlist[parseInt]

		while (getMonth != Selected) {
			WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Datepicker Previous Month'))

			getMonth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Datepicker current month'), FailureHandling.STOP_ON_FAILURE)
		}

		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/DatePicker Open Year'))

		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Datepicker Choose Year', [('year') : Year ]))
		WebUI.delay(1)

		// Select Date
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Datepicker Choose Day', [('day') : Day ]))

	}

}
