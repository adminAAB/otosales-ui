
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import java.util.ArrayList

import java.lang.Boolean

import com.kms.katalon.core.testobject.TestObject


def static "general.master.getDateToday"(
    	String format	) {
    (new general.master()).getDateToday(
        	format)
}

def static "general.master.removeCurrency"(
    	String money	) {
    (new general.master()).removeCurrency(
        	money)
}

def static "general.master.removeTrailingZeros"(
    	String money	) {
    (new general.master()).removeTrailingZeros(
        	money)
}

def static "general.master.listToString"(
    	ArrayList data	
     , 	String divider	) {
    (new general.master()).listToString(
        	data
         , 	divider)
}

def static "general.master.readStickerFile"(
    	String filePath	) {
    (new general.master()).readStickerFile(
        	filePath)
}

def static "general.master.readStickerFileRange"(
    	String filePath	
     , 	int startFrom	
     , 	int end	) {
    (new general.master()).readStickerFileRange(
        	filePath
         , 	startFrom
         , 	end)
}

def static "general.master.getLatestFilePath"() {
    (new general.master()).getLatestFilePath()
}

def static "general.appium.startServer"() {
    (new general.appium()).startServer()
}

def static "general.appium.startDevice"(
    	String port	
     , 	String appPath	
     , 	Boolean noReset	) {
    (new general.appium()).startDevice(
        	port
         , 	appPath
         , 	noReset)
}

def static "general.appium.startDevice"(
    	String port	
     , 	String appPath	
     , 	Boolean noReset	
     , 	String appPackage	
     , 	String appActivity	) {
    (new general.appium()).startDevice(
        	port
         , 	appPath
         , 	noReset
         , 	appPackage
         , 	appActivity)
}

def static "general.appium.startDevice"(
    	String port	
     , 	String appPath	
     , 	Boolean noReset	
     , 	String appPackage	
     , 	String appActivity	
     , 	String language	) {
    (new general.appium()).startDevice(
        	port
         , 	appPath
         , 	noReset
         , 	appPackage
         , 	appActivity
         , 	language)
}

def static "general.appium.stopServer"() {
    (new general.appium()).stopServer()
}

def static "com.otosurvey.FLPressEnter.pressKey"(
    	int androidKeyCode	) {
    (new com.otosurvey.FLPressEnter()).pressKey(
        	androidKeyCode)
}

def static "com.keyword.GEN5.SideMenu"(
    	String ParentMenu	
     , 	String ChildMenu	) {
    (new com.keyword.GEN5()).SideMenu(
        	ParentMenu
         , 	ChildMenu)
}

def static "com.keyword.GEN5.DatePicker"(
    	String DateNow	
     , 	TestObject DatePickerDiv	
     , 	boolean template	) {
    (new com.keyword.GEN5()).DatePicker(
        	DateNow
         , 	DatePickerDiv
         , 	template)
}

def static "com.keyword.GEN5.getAllColumnValue"(
    	TestObject tableXpath	
     , 	String gridColumn	) {
    (new com.keyword.GEN5()).getAllColumnValue(
        	tableXpath
         , 	gridColumn)
}

def static "com.keyword.GEN5.getAllRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.GEN5()).getAllRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "com.keyword.GEN5.getAllDataTable"(
    	TestObject tableXpath	) {
    (new com.keyword.GEN5()).getAllDataTable(
        	tableXpath)
}

def static "com.keyword.GEN5.getAllDataTableMultiPage"(
    	TestObject tableXpath	
     , 	TestObject ButtonNext	) {
    (new com.keyword.GEN5()).getAllDataTableMultiPage(
        	tableXpath
         , 	ButtonNext)
}

def static "com.keyword.GEN5.CompareRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	ArrayList RowsCompare	) {
    (new com.keyword.GEN5()).CompareRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	RowsCompare)
}

def static "com.keyword.GEN5.CompareColumnsValue"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	ArrayList CompareColumn	) {
    (new com.keyword.GEN5()).CompareColumnsValue(
        	tableXpath
         , 	gridColumn
         , 	CompareColumn)
}

def static "com.keyword.GEN5.compareAllTabletoDatabase"(
    	TestObject tableXpath	
     , 	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.GEN5()).compareAllTabletoDatabase(
        	tableXpath
         , 	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.GEN5.ClickExpectedRow"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.GEN5()).ClickExpectedRow(
        	tableXpath
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.GEN5.ClickExpectedRowWithNext"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	
     , 	TestObject ButtonNext	) {
    (new com.keyword.GEN5()).ClickExpectedRowWithNext(
        	tableXpath
         , 	gridColumn
         , 	columnValue
         , 	ButtonNext)
}

def static "com.keyword.GEN5.ProcessingCommand"() {
    (new com.keyword.GEN5()).ProcessingCommand()
}

def static "com.keyword.GEN5.CompareColumnToDatabase"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	String getColumn	) {
    (new com.keyword.GEN5()).CompareColumnToDatabase(
        	tableXpath
         , 	gridColumn
         , 	url
         , 	dbname
         , 	queryTable
         , 	getColumn)
}

def static "com.keyword.GEN5.CompareRowToDatabase"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.GEN5()).CompareRowToDatabase(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.GEN5.InsertIntoDataHealth"(
    	String type	
     , 	String AppName	
     , 	String value	) {
    (new com.keyword.GEN5()).InsertIntoDataHealth(
        	type
         , 	AppName
         , 	value)
}

def static "com.keyword.GEN5.getDataFromDataHealth"(
    	String Parameter	
     , 	String ParamValue	
     , 	String Value	) {
    (new com.keyword.GEN5()).getDataFromDataHealth(
        	Parameter
         , 	ParamValue
         , 	Value)
}

def static "com.keyword.GEN5.getPopUpText"(
    	TestObject TextOnPopup	) {
    (new com.keyword.GEN5()).getPopUpText(
        	TextOnPopup)
}

def static "com.keyword.GEN5.tickAllCheckboxInTable"(
    	TestObject Object	) {
    (new com.keyword.GEN5()).tickAllCheckboxInTable(
        	Object)
}

def static "com.keyword.GEN5.tickExpectedCheckbox"(
    	TestObject Table	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.GEN5()).tickExpectedCheckbox(
        	Table
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.GEN5.HealthCheckStatus"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.GEN5()).HealthCheckStatus(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "general.mobile.getDeviceInfo"(
    	String currentPlatform	
     , 	String param	) {
    (new general.mobile()).getDeviceInfo(
        	currentPlatform
         , 	param)
}

def static "general.mobile.takeScreenshot"(
    	String filename	) {
    (new general.mobile()).takeScreenshot(
        	filename)
}

def static "general.mobile.takeWebScreenshot"(
    	String filename	) {
    (new general.mobile()).takeWebScreenshot(
        	filename)
}

def static "general.mobile.clickPortion"(
    	float x	
     , 	float y	) {
    (new general.mobile()).clickPortion(
        	x
         , 	y)
}

def static "general.mobile.swipePortion"(
    	float startX	
     , 	float startY	
     , 	float endX	
     , 	float endY	) {
    (new general.mobile()).swipePortion(
        	startX
         , 	startY
         , 	endX
         , 	endY)
}

def static "general.mobile.clickPivot"(
    	TestObject objectWanted	
     , 	String pivot	
     , 	int pixelX	
     , 	int pixelY	) {
    (new general.mobile()).clickPivot(
        	objectWanted
         , 	pivot
         , 	pixelX
         , 	pixelY)
}

def static "general.mobile.scrollSearch"(
    	TestObject objectWanted	
     , 	float startX	
     , 	float startY	
     , 	float endX	
     , 	float endY	
     , 	int maxScroll	) {
    (new general.mobile()).scrollSearch(
        	objectWanted
         , 	startX
         , 	startY
         , 	endX
         , 	endY
         , 	maxScroll)
}

def static "general.mobile.scrollSearchAvoidBottomPanel"(
    	TestObject objectWanted	
     , 	float startX	
     , 	float startY	
     , 	float endX	
     , 	float endY	
     , 	int maxScroll	
     , 	String panelName	) {
    (new general.mobile()).scrollSearchAvoidBottomPanel(
        	objectWanted
         , 	startX
         , 	startY
         , 	endX
         , 	endY
         , 	maxScroll
         , 	panelName)
}

def static "general.mobile.scrollDownScreenShotUntil"(
    	TestObject objectWanted	
     , 	String filename	
     , 	float startY	
     , 	float endY	
     , 	int maxScroll	) {
    (new general.mobile()).scrollDownScreenShotUntil(
        	objectWanted
         , 	filename
         , 	startY
         , 	endY
         , 	maxScroll)
}

def static "general.mobile.scrollMultiple"(
    	float startX	
     , 	float startY	
     , 	float endX	
     , 	float endY	
     , 	int maxScroll	) {
    (new general.mobile()).scrollMultiple(
        	startX
         , 	startY
         , 	endX
         , 	endY
         , 	maxScroll)
}

def static "general.mobile.waitDisappear"(
    	TestObject objectWanted	
     , 	int timeSeconds	) {
    (new general.mobile()).waitDisappear(
        	objectWanted
         , 	timeSeconds)
}

def static "general.mobile.writeShell"(
    	TestObject objectWanted	
     , 	String inputText	) {
    (new general.mobile()).writeShell(
        	objectWanted
         , 	inputText)
}

def static "general.mobile.clickLastChild"(
    	TestObject objectWanted	) {
    (new general.mobile()).clickLastChild(
        	objectWanted)
}

def static "general.mobile.selectListItemByLabelContains"(
    	TestObject objectWanted	
     , 	String text	) {
    (new general.mobile()).selectListItemByLabelContains(
        	objectWanted
         , 	text)
}

def static "general.mobile.selectListItemByLabelandScroll"(
    	TestObject objectWanted	
     , 	String text	
     , 	float startX	
     , 	float startY	
     , 	float endX	
     , 	float endY	
     , 	int maxScroll	) {
    (new general.mobile()).selectListItemByLabelandScroll(
        	objectWanted
         , 	text
         , 	startX
         , 	startY
         , 	endX
         , 	endY
         , 	maxScroll)
}

def static "general.mobile.writeLetterByLetter"(
    	TestObject objectWanted	
     , 	String input	) {
    (new general.mobile()).writeLetterByLetter(
        	objectWanted
         , 	input)
}

def static "general.mobile.datePicker"(
    	String date	) {
    (new general.mobile()).datePicker(
        	date)
}

def static "general.mobile.captureToast"(
    	int timeout	) {
    (new general.mobile()).captureToast(
        	timeout)
}

def static "general.mobile.waitTextAppear"(
    	TestObject objectWanted	
     , 	int timeout	) {
    (new general.mobile()).waitTextAppear(
        	objectWanted
         , 	timeout)
}

def static "general.mobile.toggleWifi"() {
    (new general.mobile()).toggleWifi()
}

def static "general.mobile.takePhotobyDevice"() {
    (new general.mobile()).takePhotobyDevice()
}

def static "com.keyword.UI.verifyStaging"() {
    (new com.keyword.UI()).verifyStaging()
}

def static "com.keyword.UI.connectDB"(
    	String IP	
     , 	String dbname	) {
    (new com.keyword.UI()).connectDB(
        	IP
         , 	dbname)
}

def static "com.keyword.UI.executeQuery"(
    	String queryString	) {
    (new com.keyword.UI()).executeQuery(
        	queryString)
}

def static "com.keyword.UI.CallableState"(
    	String queryString	) {
    (new com.keyword.UI()).CallableState(
        	queryString)
}

def static "com.keyword.UI.execute"(
    	String queryString	) {
    (new com.keyword.UI()).execute(
        	queryString)
}

def static "com.keyword.UI.countdbColumn"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbColumn(
        	queryTable)
}

def static "com.keyword.UI.countdbRow"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbRow(
        	queryTable)
}

def static "com.keyword.UI.getValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getValueDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.updateValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String updateQuery	) {
    (new com.keyword.UI()).updateValueDatabase(
        	IP
         , 	dbname
         , 	updateQuery)
}

def static "com.keyword.UI.getOneRowDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getOneRowDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getOneColumnDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getOneColumnDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.getAllDataDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getAllDataDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getSpecificDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	int row	
     , 	int column	) {
    (new com.keyword.UI()).getSpecificDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	row
         , 	column)
}

def static "com.keyword.UI.compareRowDBtoArray"(
    	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList listData	) {
    (new com.keyword.UI()).compareRowDBtoArray(
        	url
         , 	dbname
         , 	queryTable
         , 	listData)
}

def static "com.keyword.UI.CompareFieldtoDatabase"(
    	ArrayList ObjRep	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).CompareFieldtoDatabase(
        	ObjRep
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getFieldsValue"(
    	ArrayList ObjRep	) {
    (new com.keyword.UI()).getFieldsValue(
        	ObjRep)
}

def static "com.keyword.UI.closeDatabaseConnection"() {
    (new com.keyword.UI()).closeDatabaseConnection()
}

def static "com.keyword.UI.newTestObject"(
    	String locator	) {
    (new com.keyword.UI()).newTestObject(
        	locator)
}

def static "com.keyword.UI.AccessURL"(
    	String App	) {
    (new com.keyword.UI()).AccessURL(
        	App)
}

def static "com.keyword.UI.AccessBrowser"(
    	String URL	) {
    (new com.keyword.UI()).AccessBrowser(
        	URL)
}

def static "com.keyword.UI.Sleep"(
    	int timeOut	) {
    (new com.keyword.UI()).Sleep(
        	timeOut)
}

def static "com.keyword.UI.Write"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).Write(
        	xpath
         , 	text)
}

def static "com.keyword.UI.WaitElement"(
    	TestObject xpath	) {
    (new com.keyword.UI()).WaitElement(
        	xpath)
}

def static "com.keyword.UI.Click"(
    	TestObject xpath	) {
    (new com.keyword.UI()).Click(
        	xpath)
}

def static "com.keyword.UI.DoubleClick"(
    	TestObject xpath	) {
    (new com.keyword.UI()).DoubleClick(
        	xpath)
}

def static "com.keyword.UI.DragAndDrop"(
    	TestObject sourceXpath	
     , 	TestObject destinationXpath	) {
    (new com.keyword.UI()).DragAndDrop(
        	sourceXpath
         , 	destinationXpath)
}

def static "com.keyword.UI.Back"() {
    (new com.keyword.UI()).Back()
}

def static "com.keyword.UI.HoverItem"(
    	TestObject xpath	) {
    (new com.keyword.UI()).HoverItem(
        	xpath)
}

def static "com.keyword.UI.DeleteWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).DeleteWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.SkipWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).SkipWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.ComboBoxSearch"(
    	TestObject Combo1	
     , 	TestObject Combo2	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearch(
        	Combo1
         , 	Combo2
         , 	Value)
}

def static "com.keyword.UI.ComboBoxSearchSkip"(
    	TestObject comboOpen	
     , 	TestObject comboSearch	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearchSkip(
        	comboOpen
         , 	comboSearch
         , 	Value)
}

def static "com.keyword.UI.ComboBox"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBox(
        	Combo
         , 	Value)
}

def static "com.keyword.UI.MultiSelectComboBox"(
    	TestObject Combo	
     , 	String Value1	
     , 	String Value2	
     , 	String Value3	
     , 	String Value4	) {
    (new com.keyword.UI()).MultiSelectComboBox(
        	Combo
         , 	Value1
         , 	Value2
         , 	Value3
         , 	Value4)
}

def static "com.keyword.UI.CheckDisableandWrite"(
    	TestObject Xpath	
     , 	String text	) {
    (new com.keyword.UI()).CheckDisableandWrite(
        	Xpath
         , 	text)
}

def static "com.keyword.UI.RunningPhoneNumber"(
    	TestObject xpath	) {
    (new com.keyword.UI()).RunningPhoneNumber(
        	xpath)
}

def static "com.keyword.UI.UploadFile"(
    	String fileLocation	
     , 	String pictureName	) {
    (new com.keyword.UI()).UploadFile(
        	fileLocation
         , 	pictureName)
}

def static "com.keyword.UI.UploadFile2"(
    	String pictureName	) {
    (new com.keyword.UI()).UploadFile2(
        	pictureName)
}

def static "com.keyword.UI.RunScheduler"(
    	String path	) {
    (new com.keyword.UI()).RunScheduler(
        	path)
}

def static "com.keyword.UI.WriteAllRowsXls"(
    	String path	
     , 	int row	
     , 	ArrayList value	) {
    (new com.keyword.UI()).WriteAllRowsXls(
        	path
         , 	row
         , 	value)
}

def static "com.keyword.UI.WriteSingleCellXls"(
    	String path	
     , 	int row	
     , 	int column	
     , 	Object value	) {
    (new com.keyword.UI()).WriteSingleCellXls(
        	path
         , 	row
         , 	column
         , 	value)
}

def static "com.keyword.UI.AccessURLwithPlugin"(
    	String url	
     , 	String Plugin	) {
    (new com.keyword.UI()).AccessURLwithPlugin(
        	url
         , 	Plugin)
}

def static "com.keyword.UI.readQRCode"() {
    (new com.keyword.UI()).readQRCode()
}

def static "com.keyword.UI.getDateToday"(
    	String format	) {
    (new com.keyword.UI()).getDateToday(
        	format)
}

def static "com.keyword.UI.ScreenShot"(
    	String FileName	) {
    (new com.keyword.UI()).ScreenShot(
        	FileName)
}

def static "com.keyword.UI.GlobalVar"(
    	String name	
     , 	Object value	) {
    (new com.keyword.UI()).GlobalVar(
        	name
         , 	value)
}

def static "com.keyword.UI.SetGlobal"(
    	String varName	
     , 	String App	) {
    (new com.keyword.UI()).SetGlobal(
        	varName
         , 	App)
}

def static "com.keyword.UI.Note"(
    	Object variable	) {
    (new com.keyword.UI()).Note(
        	variable)
}

def static "general.database.connectLiTT"() {
    (new general.database()).connectLiTT()
}

def static "general.database.connectAAB"() {
    (new general.database()).connectAAB()
}

def static "general.database.connectDB"(
    	String url	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new general.database()).connectDB(
        	url
         , 	dbname
         , 	username
         , 	password)
}

def static "general.database.executeQuery"(
    	String queryString	) {
    (new general.database()).executeQuery(
        	queryString)
}

def static "general.database.execute"(
    	String queryString	) {
    (new general.database()).execute(
        	queryString)
}

def static "general.database.closeDatabaseConnection"() {
    (new general.database()).closeDatabaseConnection()
}

def static "com.otosurvey.DBConnection.connectDB"(
    	String url	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new com.otosurvey.DBConnection()).connectDB(
        	url
         , 	dbname
         , 	username
         , 	password)
}

def static "com.otosurvey.DBConnection.executeQuery"(
    	String queryString	) {
    (new com.otosurvey.DBConnection()).executeQuery(
        	queryString)
}

def static "com.otosurvey.DBConnection.closeDatabaseConnection"() {
    (new com.otosurvey.DBConnection()).closeDatabaseConnection()
}

def static "com.otosurvey.DBConnection.execute"(
    	String queryString	) {
    (new com.otosurvey.DBConnection()).execute(
        	queryString)
}

def static "general.scheduler.Otosales_CreateOrder"() {
    (new general.scheduler()).Otosales_CreateOrder()
}

def static "general.scheduler.Otosales_MonitorOrder"() {
    (new general.scheduler()).Otosales_MonitorOrder()
}

def static "general.scheduler.Otosales_Approval_Renew"() {
    (new general.scheduler()).Otosales_Approval_Renew()
}

def static "general.scheduler.Otosales_Approval_New"() {
    (new general.scheduler()).Otosales_Approval_New()
}

def static "general.scheduler.Otosales_Approval_Next_Limit"() {
    (new general.scheduler()).Otosales_Approval_Next_Limit()
}

def static "general.scheduler.Otosales_Approval_Gen5"() {
    (new general.scheduler()).Otosales_Approval_Gen5()
}

def static "com.otosurvey.FLUpdateFLag.connectDB"(
    	String server	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new com.otosurvey.FLUpdateFLag()).connectDB(
        	server
         , 	dbname
         , 	username
         , 	password)
}

def static "com.otosurvey.FLUpdateFLag.executeQuery"(
    	String queryString	) {
    (new com.otosurvey.FLUpdateFLag()).executeQuery(
        	queryString)
}

def static "com.otosurvey.FLUpdateFLag.closeDatabaseConnection"() {
    (new com.otosurvey.FLUpdateFLag()).closeDatabaseConnection()
}

def static "com.otosurvey.FLUpdateFLag.executeFlagLK"(
    	String LK	) {
    (new com.otosurvey.FLUpdateFLag()).executeFlagLK(
        	LK)
}

def static "general.web.waitProcessingCommand"() {
    (new general.web()).waitProcessingCommand()
}

def static "general.web.uploadImage1"(
    	String fileLocation	
     , 	String pictureName	) {
    (new general.web()).uploadImage1(
        	fileLocation
         , 	pictureName)
}

def static "general.web.addRegistration"(
    	String Region	
     , 	String Registration	) {
    (new general.web()).addRegistration(
        	Region
         , 	Registration)
}

def static "general.web.addChassis"(
    	String Region	
     , 	String Chassis	) {
    (new general.web()).addChassis(
        	Region
         , 	Chassis)
}

def static "general.web.addEngine"(
    	String Region	
     , 	String Engine	) {
    (new general.web()).addEngine(
        	Region
         , 	Engine)
}

def static "general.web.getInteger"(
    	String Input	) {
    (new general.web()).getInteger(
        	Input)
}

def static "general.web.addRegistrationGodig"(
    	String Region	
     , 	String Registration	) {
    (new general.web()).addRegistrationGodig(
        	Region
         , 	Registration)
}

def static "general.web.addChassisGodig"(
    	String Region	
     , 	String Chassis	) {
    (new general.web()).addChassisGodig(
        	Region
         , 	Chassis)
}

def static "general.web.addEngineGodig"(
    	String Region	
     , 	String Engine	) {
    (new general.web()).addEngineGodig(
        	Region
         , 	Engine)
}
