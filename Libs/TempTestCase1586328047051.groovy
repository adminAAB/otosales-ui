import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\HRM\\AppData\\Local\\Temp\\Katalon\\Test Cases\\SCENARIO\\OTOSALES\\Collaboration\\NEW ORDER\\MVNONGODIG\\MULTI YEAR\\TELESALES\\SKIP SURVEY\\INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY\\20200408_134047\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/MULTI YEAR/TELESALES/SKIP SURVEY/INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY', new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/MULTI YEAR/TELESALES/SKIP SURVEY/INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
