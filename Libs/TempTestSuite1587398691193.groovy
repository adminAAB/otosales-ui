import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY')

suiteProperties.put('name', 'INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("D:\\Repository\\github\\Otosales-UI\\Reports\\OTOSALES\\NEW ORDER\\MVNONGODIG\\MULTI YEAR\\INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY\\20200420_230451\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP2Y,M200,FULLCVR,COMPANY', suiteProperties, [new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/MULTI YEAR/TELESALES/SKIP SURVEY/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY', 'Test Cases/SCENARIO/OTOSALES/Collaboration/NEW ORDER/MVNONGODIG/MULTI YEAR/TELESALES/SKIP SURVEY/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY',  null), new TestCaseBinding('Test Cases/SCENARIO/GEN5/TASKLIST SA/Tasklist', 'Test Cases/SCENARIO/GEN5/TASKLIST SA/Tasklist',  null), new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/APPROVAL/Approval', 'Test Cases/SCENARIO/OTOSALES/APPROVAL/Approval',  null)])
