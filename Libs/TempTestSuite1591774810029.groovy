import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Request Roy/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - AO - EPH')

suiteProperties.put('name', 'INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - AO - EPH')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("D:\\GitHub\\Otosales\\Otosales-UI\\Reports\\20200610_144009\\OTOSALES\\NEW ORDER\\MVNONGODIG\\SINGLE YEAR\\Request Roy\\INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - AO - EPH\\20200610_144009\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/Request Roy/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - AO - EPH', suiteProperties, new File("D:\\GitHub\\Otosales\\Otosales-UI\\Reports\\20200610_144009\\OTOSALES\\NEW ORDER\\MVNONGODIG\\SINGLE YEAR\\Request Roy\\INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY - AO - EPH\\20200610_144009\\testCaseBinding"))
