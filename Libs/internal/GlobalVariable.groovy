package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object LongestDelay
     
    /**
     * <p></p>
     */
    public static Object MediumDelay
     
    /**
     * <p></p>
     */
    public static Object DefaultDelay
     
    /**
     * <p></p>
     */
    public static Object DownloadFolder
     
    /**
     * <p>Profile default : Diubah sesuai destinasi appium</p>
     */
    public static Object AppiumPath
     
    /**
     * <p></p>
     */
    public static Object AppiumPort
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object CurrentLanguage
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object CurrentDevice
     
    /**
     * <p></p>
     */
    public static Object ScreenshotPath
     
    /**
     * <p>Profile default : Debug, Release</p>
     */
    public static Object ScreenshotFolder
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotPhone
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotTS
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotUrutan
     
    /**
     * <p></p>
     */
    public static Object ScreenshotSet
     
    /**
     * <p></p>
     */
    public static Object ScreenshotTime
     
    /**
     * <p>Profile default : Diubah sesuai destinasi file</p>
     */
    public static Object OtosurveyPath
     
    /**
     * <p>Profile default : Diubah sesuai destinasi file</p>
     */
    public static Object OtocarePath
     
    /**
     * <p></p>
     */
    public static Object intNumCap
     
    /**
     * <p></p>
     */
    public static Object intIdAddPanel_LK
     
    /**
     * <p></p>
     */
    public static Object dataPanel
     
    /**
     * <p></p>
     */
    public static Object intColoumn
     
    /**
     * <p></p>
     */
    public static Object SavedValue1
     
    /**
     * <p></p>
     */
    public static Object SavedValue2
     
    /**
     * <p></p>
     */
    public static Object StickerID
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object URLOtosalesCentos
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object URLOtosales
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object URLGen5
     
    /**
     * <p></p>
     */
    public static Object RegistrationNo
     
    /**
     * <p></p>
     */
    public static Object ChassisNo
     
    /**
     * <p></p>
     */
    public static Object EngineNo
     
    /**
     * <p></p>
     */
    public static Object ProspectName
     
    /**
     * <p></p>
     */
    public static Object GodigPrefix
     
    /**
     * <p></p>
     */
    public static Object GodigVANumber
     
    /**
     * <p></p>
     */
    public static Object GodigPrice
     
    /**
     * <p></p>
     */
    public static Object GodigOrderNo
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileLoc
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileName
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileDestination
     
    /**
     * <p></p>
     */
    public static Object ShortenURL
     
    /**
     * <p></p>
     */
    public static Object CustPhone
     
    /**
     * <p></p>
     */
    public static Object CustVehicle
     
    /**
     * <p></p>
     */
    public static Object CustUsage
     
    /**
     * <p></p>
     */
    public static Object CustRegion
     
    /**
     * <p></p>
     */
    public static Object ProductType
     
    /**
     * <p></p>
     */
    public static Object ProductCode
     
    /**
     * <p></p>
     */
    public static Object Coverage
     
    /**
     * <p></p>
     */
    public static Object PremiTJH
     
    /**
     * <p></p>
     */
    public static Object PremiSRCC
     
    /**
     * <p></p>
     */
    public static Object PremiFlood
     
    /**
     * <p></p>
     */
    public static Object PremiEarthquake
     
    /**
     * <p></p>
     */
    public static Object PremiTerrorism
     
    /**
     * <p></p>
     */
    public static Object PremiDriver
     
    /**
     * <p></p>
     */
    public static Object PremiPassanger
     
    /**
     * <p></p>
     */
    public static Object PremiAccessories
     
    /**
     * <p></p>
     */
    public static Object OtosalesPolicyNo
     
    /**
     * <p></p>
     */
    public static Object OtosalesOrderNo
     
    /**
     * <p></p>
     */
    public static Object OtosalesOldPolicyNo
     
    /**
     * <p>Profile default : Not Used Yet</p>
     */
    public static Object Name
     
    /**
     * <p></p>
     */
    public static Object type
     
    /**
     * <p></p>
     */
    public static Object OtosalesGrossPremi
     
    /**
     * <p></p>
     */
    public static Object OrderType
     
    /**
     * <p></p>
     */
    public static Object OtosalesLogin
     
    /**
     * <p></p>
     */
    public static Object CashDealer
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object Otosales
     
    /**
     * <p></p>
     */
    public static Object LinkPaymentFullPayment
     
    /**
     * <p></p>
     */
    public static Object LinkPaymentInstallment
     
    /**
     * <p></p>
     */
    public static Object URLMidtrans
     
    /**
     * <p></p>
     */
    public static Object PolicyNo
     
    /**
     * <p></p>
     */
    public static Object SpecialMenu
     
    /**
     * <p></p>
     */
    public static Object Region
     
    /**
     * <p></p>
     */
    public static Object URLType
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object OtosalesStaging
     
    /**
     * <p></p>
     */
    public static Object OtosalesQC
     
    /**
     * <p></p>
     */
    public static Object Gen5Staging
     
    /**
     * <p></p>
     */
    public static Object Gen5QC
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            LongestDelay = selectedVariables['LongestDelay']
            MediumDelay = selectedVariables['MediumDelay']
            DefaultDelay = selectedVariables['DefaultDelay']
            DownloadFolder = selectedVariables['DownloadFolder']
            AppiumPath = selectedVariables['AppiumPath']
            AppiumPort = selectedVariables['AppiumPort']
            CurrentLanguage = selectedVariables['CurrentLanguage']
            CurrentDevice = selectedVariables['CurrentDevice']
            ScreenshotPath = selectedVariables['ScreenshotPath']
            ScreenshotFolder = selectedVariables['ScreenshotFolder']
            ScreenshotPhone = selectedVariables['ScreenshotPhone']
            ScreenshotTS = selectedVariables['ScreenshotTS']
            ScreenshotUrutan = selectedVariables['ScreenshotUrutan']
            ScreenshotSet = selectedVariables['ScreenshotSet']
            ScreenshotTime = selectedVariables['ScreenshotTime']
            OtosurveyPath = selectedVariables['OtosurveyPath']
            OtocarePath = selectedVariables['OtocarePath']
            intNumCap = selectedVariables['intNumCap']
            intIdAddPanel_LK = selectedVariables['intIdAddPanel_LK']
            dataPanel = selectedVariables['dataPanel']
            intColoumn = selectedVariables['intColoumn']
            SavedValue1 = selectedVariables['SavedValue1']
            SavedValue2 = selectedVariables['SavedValue2']
            StickerID = selectedVariables['StickerID']
            URLOtosalesCentos = selectedVariables['URLOtosalesCentos']
            URLOtosales = selectedVariables['URLOtosales']
            URLGen5 = selectedVariables['URLGen5']
            RegistrationNo = selectedVariables['RegistrationNo']
            ChassisNo = selectedVariables['ChassisNo']
            EngineNo = selectedVariables['EngineNo']
            ProspectName = selectedVariables['ProspectName']
            GodigPrefix = selectedVariables['GodigPrefix']
            GodigVANumber = selectedVariables['GodigVANumber']
            GodigPrice = selectedVariables['GodigPrice']
            GodigOrderNo = selectedVariables['GodigOrderNo']
            uploadFileLoc = selectedVariables['uploadFileLoc']
            uploadFileName = selectedVariables['uploadFileName']
            uploadFileDestination = selectedVariables['uploadFileDestination']
            ShortenURL = selectedVariables['ShortenURL']
            CustPhone = selectedVariables['CustPhone']
            CustVehicle = selectedVariables['CustVehicle']
            CustUsage = selectedVariables['CustUsage']
            CustRegion = selectedVariables['CustRegion']
            ProductType = selectedVariables['ProductType']
            ProductCode = selectedVariables['ProductCode']
            Coverage = selectedVariables['Coverage']
            PremiTJH = selectedVariables['PremiTJH']
            PremiSRCC = selectedVariables['PremiSRCC']
            PremiFlood = selectedVariables['PremiFlood']
            PremiEarthquake = selectedVariables['PremiEarthquake']
            PremiTerrorism = selectedVariables['PremiTerrorism']
            PremiDriver = selectedVariables['PremiDriver']
            PremiPassanger = selectedVariables['PremiPassanger']
            PremiAccessories = selectedVariables['PremiAccessories']
            OtosalesPolicyNo = selectedVariables['OtosalesPolicyNo']
            OtosalesOrderNo = selectedVariables['OtosalesOrderNo']
            OtosalesOldPolicyNo = selectedVariables['OtosalesOldPolicyNo']
            Name = selectedVariables['Name']
            type = selectedVariables['type']
            OtosalesGrossPremi = selectedVariables['OtosalesGrossPremi']
            OrderType = selectedVariables['OrderType']
            OtosalesLogin = selectedVariables['OtosalesLogin']
            CashDealer = selectedVariables['CashDealer']
            Otosales = selectedVariables['Otosales']
            LinkPaymentFullPayment = selectedVariables['LinkPaymentFullPayment']
            LinkPaymentInstallment = selectedVariables['LinkPaymentInstallment']
            URLMidtrans = selectedVariables['URLMidtrans']
            PolicyNo = selectedVariables['PolicyNo']
            SpecialMenu = selectedVariables['SpecialMenu']
            Region = selectedVariables['Region']
            URLType = selectedVariables['URLType']
            OtosalesStaging = selectedVariables['OtosalesStaging']
            OtosalesQC = selectedVariables['OtosalesQC']
            Gen5Staging = selectedVariables['Gen5Staging']
            Gen5QC = selectedVariables['Gen5QC']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
