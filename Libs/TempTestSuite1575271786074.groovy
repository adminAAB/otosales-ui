import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/OTOSALES/NEW ORDER/Order New - Compre 1 th, TSI dibawah 200jt, Telerenewal, Full coverage')

suiteProperties.put('name', 'Order New - Compre 1 th, TSI dibawah 200jt, Telerenewal, Full coverage')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("D:\\Repository\\Otosales\\Otosales - General\\Reports\\OTOSALES\\NEW ORDER\\Order New - Compre 1 th, TSI dibawah 200jt, Telerenewal, Full coverage\\20191202_142946\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/OTOSALES/NEW ORDER/Order New - Compre 1 th, TSI dibawah 200jt, Telerenewal, Full coverage', suiteProperties, [new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/LOGIN/Login AO', 'Test Cases/SCENARIO/OTOSALES/LOGIN/Login AO',  null), new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/NEW/Input Prospect', 'Test Cases/SCENARIO/OTOSALES/NEW/Input Prospect',  null), new TestCaseBinding('Test Cases/SCENARIO/OTOSALES/NEW/Home', 'Test Cases/SCENARIO/OTOSALES/NEW/Home',  null)])
