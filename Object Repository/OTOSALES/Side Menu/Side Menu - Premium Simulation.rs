<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu - Premium Simulation</name>
   <tag></tag>
   <elementGuidId>ae87e342-2f70-4565-a130-777fa90ac94c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//aside//a[@href=&quot;/premiumsimulation&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//aside//a[@href=&quot;/premiumsimulation&quot;])[2]</value>
   </webElementProperties>
</WebElementEntity>
