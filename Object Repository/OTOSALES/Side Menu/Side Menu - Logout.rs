<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu - Logout</name>
   <tag></tag>
   <elementGuidId>7a3da660-5836-4eb0-a8f6-54a0e5742e91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[contains(text(),'Logout')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[contains(text(),'Logout')])[2]</value>
   </webElementProperties>
</WebElementEntity>
