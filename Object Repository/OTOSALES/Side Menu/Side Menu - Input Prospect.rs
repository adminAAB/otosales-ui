<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu - Input Prospect</name>
   <tag></tag>
   <elementGuidId>81fd1b34-8bef-4fc4-bd4d-cee9a1c4746c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//aside//a[@href=&quot;/inputprospect&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//aside//a[@href=&quot;/inputprospect&quot;])[2]</value>
   </webElementProperties>
</WebElementEntity>
