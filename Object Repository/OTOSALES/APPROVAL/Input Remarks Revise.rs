<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Remarks Revise</name>
   <tag></tag>
   <elementGuidId>19213843-a31a-41b1-94de-e7932ae42e3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[text()=&quot;Remarks&quot;]//following-sibling::textarea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[text()=&quot;Remarks&quot;]//following-sibling::textarea</value>
   </webElementProperties>
</WebElementEntity>
