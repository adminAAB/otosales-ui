<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Remarks Info Yes</name>
   <tag></tag>
   <elementGuidId>5750cd5f-7720-40fb-84df-75cc2d09f2fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[text()=&quot;Information&quot;]//ancestor::div[2]//button[text()=&quot;Yes&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[text()=&quot;Information&quot;]//ancestor::div[2]//button[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
</WebElementEntity>
