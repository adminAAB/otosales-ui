<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Approval - Approve</name>
   <tag></tag>
   <elementGuidId>df2f9e83-8c12-45c6-bea3-e34c0db52cee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[text()=&quot;Order Approval&quot;]//ancestor::div[2]//button[text()=&quot;${chooseAgain}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[text()=&quot;Order Approval&quot;]//ancestor::div[2]//button[text()=&quot;${chooseAgain}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
