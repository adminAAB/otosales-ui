<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload SPPAKB image</name>
   <tag></tag>
   <elementGuidId>c2b62f03-bf1b-4d9d-b944-be453905c4c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'SPPAK')]//parent::div//img[@class='contentimg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'SPPAK')]//parent::div//img[@class='contentimg']</value>
   </webElementProperties>
</WebElementEntity>
