<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Image</name>
   <tag></tag>
   <elementGuidId>818a804d-4636-4832-a83c-6052c945a9b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext labelspanbold&quot; and text()=&quot; Choose&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext labelspanbold&quot; and text()=&quot; Choose&quot;]</value>
   </webElementProperties>
</WebElementEntity>
