<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pop up Confirmation</name>
   <tag></tag>
   <elementGuidId>6c6298e2-4eeb-4583-ad4e-75d3f118aea4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[text()=&quot;Confirmation&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[text()=&quot;Confirmation&quot;]</value>
   </webElementProperties>
</WebElementEntity>
