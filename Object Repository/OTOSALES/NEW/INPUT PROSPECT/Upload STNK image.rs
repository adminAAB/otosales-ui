<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload STNK image</name>
   <tag></tag>
   <elementGuidId>d1e64210-5f99-47c9-b186-7f06c85e403c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'STNK')]//parent::div//img[@class='contentimg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'STNK')]//parent::div//img[@class='contentimg']</value>
   </webElementProperties>
</WebElementEntity>
