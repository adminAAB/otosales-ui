<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Prospect Name</name>
   <tag></tag>
   <elementGuidId>e1057a92-1fe1-4d9b-ac50-ae4a779e293f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @name=&quot;Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @name=&quot;Name&quot;]</value>
   </webElementProperties>
</WebElementEntity>
