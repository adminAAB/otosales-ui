<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Dealer Name</name>
   <tag></tag>
   <elementGuidId>532cf199-e115-4054-bfbd-355a68790885</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='userlog' and @placeholder='Ketik Select dealer name' and @name='search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='userlog' and @placeholder='Ketik Select dealer name' and @name='search']</value>
   </webElementProperties>
</WebElementEntity>
