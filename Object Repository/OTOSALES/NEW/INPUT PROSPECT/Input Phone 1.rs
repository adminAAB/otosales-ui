<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Phone 1</name>
   <tag></tag>
   <elementGuidId>345e1d35-0117-4b83-b9aa-f8ad881cceb9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;PHONE NUMBER 1&quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;PHONE NUMBER 1&quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
