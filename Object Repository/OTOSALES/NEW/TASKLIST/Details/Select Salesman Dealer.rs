<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Salesman Dealer</name>
   <tag></tag>
   <elementGuidId>08fe3cc9-b4a3-44ba-9d5b-f8d6770cd19b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Pilih Salesman Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Pilih Salesman Name&quot;]</value>
   </webElementProperties>
</WebElementEntity>
