<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text Gross Premium</name>
   <tag></tag>
   <elementGuidId>9c242535-9d1f-4faa-a2d6-c960da3a76be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Gross Premium&quot;]//following-sibling::span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Gross Premium&quot;]//following-sibling::span</value>
   </webElementProperties>
</WebElementEntity>
