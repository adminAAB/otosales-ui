<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox CC Full Payment</name>
   <tag></tag>
   <elementGuidId>d73e8273-6ca9-425a-a108-5380192426f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id=&quot;radiobtninput-paymentInfoTypeCC-FULLPAYMENT&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;radiobtninput-paymentInfoTypeCC-FULLPAYMENT&quot;]</value>
   </webElementProperties>
</WebElementEntity>
