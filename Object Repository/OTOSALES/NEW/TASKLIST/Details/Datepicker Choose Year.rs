<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Datepicker Choose Year</name>
   <tag></tag>
   <elementGuidId>d3dac985-e128-4003-be3d-3559bea1acf9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;react-datepicker&quot;]//div[contains(@class,&quot;year-option&quot;) and text()=&quot;${year}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;react-datepicker&quot;]//div[contains(@class,&quot;year-option&quot;) and text()=&quot;${year}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
