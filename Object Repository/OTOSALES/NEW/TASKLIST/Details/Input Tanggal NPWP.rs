<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Tanggal NPWP</name>
   <tag></tag>
   <elementGuidId>4af50eb6-b3f6-4d44-a81e-4ef84b96c46d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Tanggal NPWP&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Tanggal NPWP&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
