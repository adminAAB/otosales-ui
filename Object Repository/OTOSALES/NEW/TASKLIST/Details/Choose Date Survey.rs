<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Date Survey</name>
   <tag></tag>
   <elementGuidId>f2394221-c173-40df-86a6-d5079ce6dbc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(@class,&quot;today&quot;)]//following-sibling::div)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(@class,&quot;today&quot;)]//following-sibling::div)[1]</value>
   </webElementProperties>
</WebElementEntity>
