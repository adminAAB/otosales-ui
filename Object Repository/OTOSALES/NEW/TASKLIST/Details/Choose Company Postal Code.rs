<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Company Postal Code</name>
   <tag></tag>
   <elementGuidId>78ad7834-828c-435c-bb57-390690d1975b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@name=&quot;companypostalcode&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@name=&quot;companypostalcode&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
