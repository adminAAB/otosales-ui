<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Others KodePos</name>
   <tag></tag>
   <elementGuidId>db8945dd-3f90-4567-aea8-e15437a15a0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;list-group&quot;]//span[contains(text(),&quot;${other}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;list-group&quot;]//span[contains(text(),&quot;${other}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
