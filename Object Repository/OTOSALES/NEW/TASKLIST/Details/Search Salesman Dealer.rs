<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Salesman Dealer</name>
   <tag></tag>
   <elementGuidId>5e7231a6-9998-4090-bc85-c3c0e7e3d48f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='userlog' and @placeholder='Ketik Pilih Salesman Name' and @name='search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='userlog' and @placeholder='Ketik Pilih Salesman Name' and @name='search']</value>
   </webElementProperties>
</WebElementEntity>
