<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Kode Pos Prokhus</name>
   <tag></tag>
   <elementGuidId>fb836c3a-02b9-4707-93a3-8ef77f5ba93a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@name=&quot;personalkodepos&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@name=&quot;personalkodepos&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
