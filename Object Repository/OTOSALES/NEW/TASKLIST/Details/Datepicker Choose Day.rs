<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Datepicker Choose Day</name>
   <tag></tag>
   <elementGuidId>b13673b9-0cc2-4b6c-9b98-34c9b6f7665c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;react-datepicker__month&quot;]//div[not(contains(@class,&quot;outside-month&quot;)) and text()=&quot;${day}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;react-datepicker__month&quot;]//div[not(contains(@class,&quot;outside-month&quot;)) and text()=&quot;${day}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
