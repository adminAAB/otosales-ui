<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Customer Name</name>
   <tag></tag>
   <elementGuidId>152f9c68-614f-46ab-8bbd-118c461ba616</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Nama Customer&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Nama Customer&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
