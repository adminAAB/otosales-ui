<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Dealer Name</name>
   <tag></tag>
   <elementGuidId>57a91b6e-adf0-49d7-9f24-762a28d3c993</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'AUTO 2000 CILANDAK JKT')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'AUTO 2000 CILANDAK JKT')]</value>
   </webElementProperties>
</WebElementEntity>
