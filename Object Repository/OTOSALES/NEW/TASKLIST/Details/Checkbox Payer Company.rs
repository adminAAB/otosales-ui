<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Payer Company</name>
   <tag></tag>
   <elementGuidId>e9457ee1-da43-4966-98c6-b494d1d8bc0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name=&quot;personalpayercompany&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=&quot;personalpayercompany&quot;]</value>
   </webElementProperties>
</WebElementEntity>
