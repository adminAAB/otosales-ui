<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox CC Installment</name>
   <tag></tag>
   <elementGuidId>847b9b72-bf00-4495-b250-4ccf94554825</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id=&quot;radiobtninput-paymentInfoTypeCC-INSTALLMENT&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;radiobtninput-paymentInfoTypeCC-INSTALLMENT&quot;]</value>
   </webElementProperties>
</WebElementEntity>
