<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Company Name</name>
   <tag></tag>
   <elementGuidId>b04c57a1-1eab-4b25-961a-633d2c310721</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Nama Perusahaan di NPWP&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Nama Perusahaan di NPWP&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
