<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Kode Pos</name>
   <tag></tag>
   <elementGuidId>962ab754-9e80-4fb6-90ee-abfeceb2dc3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@name=&quot;personalkodepos&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@name=&quot;personalkodepos&quot;]//span[contains(text(),&quot;${postal}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
