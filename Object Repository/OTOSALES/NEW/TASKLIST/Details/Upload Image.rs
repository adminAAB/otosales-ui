<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload Image</name>
   <tag></tag>
   <elementGuidId>3b0f421e-7d2d-4263-b128-a01630ca742c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//center[contains(text(),&quot;${image}&quot;)]//preceding-sibling::div//img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//center[contains(text(),&quot;${image}&quot;)]//preceding-sibling::div//img</value>
   </webElementProperties>
</WebElementEntity>
