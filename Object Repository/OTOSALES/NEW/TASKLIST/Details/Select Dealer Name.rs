<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Dealer Name</name>
   <tag></tag>
   <elementGuidId>8c79bd7d-3e63-4453-a40e-b0d84effccd6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Pilih Dealer Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Pilih Dealer Name&quot;]</value>
   </webElementProperties>
</WebElementEntity>
