<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Jenis Kelamin Prokhus - Option</name>
   <tag></tag>
   <elementGuidId>8d7bce4c-8985-4338-98b3-7b96d644742a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[2]/div/div/div/div/div[2]/div/div/div[1]/div/div/span[text()=&quot;${sex}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[2]/div/div/div/div/div[2]/div/div/div[1]/div/div/span[text()=&quot;${sex}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
