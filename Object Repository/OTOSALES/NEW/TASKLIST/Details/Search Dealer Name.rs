<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Dealer Name</name>
   <tag></tag>
   <elementGuidId>8fadb704-bd4d-4e35-a7f8-b4faf4d93fff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='userlog' and @placeholder='Ketik Pilih Dealer Name' and @name='search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='userlog' and @placeholder='Ketik Pilih Dealer Name' and @name='search']</value>
   </webElementProperties>
</WebElementEntity>
