<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Primary CTI</name>
   <tag></tag>
   <elementGuidId>e1cc737f-5215-4387-8726-e77f4c4cef2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;Set Primary&quot;]//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;Set Primary&quot;]//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
