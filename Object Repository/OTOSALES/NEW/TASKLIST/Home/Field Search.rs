<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field Search</name>
   <tag></tag>
   <elementGuidId>f98f61e8-bd54-420f-b85c-9eb55c09edd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;Search&quot;]//following-sibling::div//input[@name=&quot;searchterm&quot;]
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;Search&quot;]//following-sibling::div//input[@name=&quot;searchterm&quot;]
</value>
   </webElementProperties>
</WebElementEntity>
