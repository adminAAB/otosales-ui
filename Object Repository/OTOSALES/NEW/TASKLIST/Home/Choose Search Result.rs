<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Search Result</name>
   <tag></tag>
   <elementGuidId>3a82a821-d7b8-42dc-b8e7-428681ee9a2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,&quot;panel-body-list&quot;) and @name=&quot;sampleList&quot;]//div[contains(text(),&quot;Need FU&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,&quot;panel-body-list&quot;) and @name=&quot;sampleList&quot;]//div[contains(text(),&quot;Need FU&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
