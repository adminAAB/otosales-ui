<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Close Filter</name>
   <tag></tag>
   <elementGuidId>f12f7a6a-5fb0-4dce-bbe9-1bf4dd533e46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class,&quot;styles_closeButton&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class,&quot;styles_closeButton&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
