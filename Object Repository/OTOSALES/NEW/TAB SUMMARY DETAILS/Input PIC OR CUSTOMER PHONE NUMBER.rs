<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input PIC OR CUSTOMER PHONE NUMBER</name>
   <tag></tag>
   <elementGuidId>f2118bac-5a75-4c30-92c1-7a431d22b708</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;PIC / CUSTOMER PHONE NUMBER * &quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;PIC / CUSTOMER PHONE NUMBER * &quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
