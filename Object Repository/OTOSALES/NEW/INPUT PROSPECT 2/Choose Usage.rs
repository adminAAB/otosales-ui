<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Usage</name>
   <tag></tag>
   <elementGuidId>d2d1f3a8-2b98-4906-87d3-a10c448807f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-body panel-body-list&quot;]//span[text()=&quot;${key2}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-body panel-body-list&quot;]//span[text()=&quot;${key2}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
