<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Region</name>
   <tag></tag>
   <elementGuidId>43c877f0-8ca5-4c41-b4aa-66d2ae5d0a2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
   </webElementProperties>
</WebElementEntity>
