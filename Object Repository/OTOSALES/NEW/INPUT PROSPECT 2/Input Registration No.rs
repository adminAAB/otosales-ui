<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Registration No</name>
   <tag></tag>
   <elementGuidId>b774817a-8dd8-4fd7-a4f5-fcd79366f9e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @name=&quot;VehicleRegistrationNumber&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @name=&quot;VehicleRegistrationNumber&quot;]</value>
   </webElementProperties>
</WebElementEntity>
