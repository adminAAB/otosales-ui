<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Kendaraan</name>
   <tag></tag>
   <elementGuidId>1af2e7d1-d108-4302-81f4-b295b620615d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key1}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key1}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
