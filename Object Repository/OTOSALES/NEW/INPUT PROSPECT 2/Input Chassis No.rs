<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Chassis No</name>
   <tag></tag>
   <elementGuidId>3388e1e9-5d29-4988-8542-70edd43c4e40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @name=&quot;VehicleChasisNumber&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @name=&quot;VehicleChasisNumber&quot;]</value>
   </webElementProperties>
</WebElementEntity>
