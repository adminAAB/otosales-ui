<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select PA Driver</name>
   <tag></tag>
   <elementGuidId>5778751c-b30a-438c-996e-f6a7d8a70d25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;PA Driver SI&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;PA Driver SI&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
