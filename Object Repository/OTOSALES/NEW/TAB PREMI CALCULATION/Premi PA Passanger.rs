<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Premi PA Passanger</name>
   <tag></tag>
   <elementGuidId>7d5a7652-a838-4dbd-9e42-766ecbd0e22e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;PASSENGER&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;PASSENGER&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
   </webElementProperties>
</WebElementEntity>
