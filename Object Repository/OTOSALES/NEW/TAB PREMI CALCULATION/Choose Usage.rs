<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Usage</name>
   <tag></tag>
   <elementGuidId>3c9540a6-b336-4e25-bd35-d21000164d21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-body panel-body-list&quot;]//span[text()=&quot;${key2}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-body panel-body-list&quot;]//span[text()=&quot;${key2}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
