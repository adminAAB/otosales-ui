<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox FLOOD - disable</name>
   <tag></tag>
   <elementGuidId>a8c42810-13e4-4276-844e-fe5a46e1dcc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsFLDChecked&quot; and @disabled]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsFLDChecked&quot; and @disabled]</value>
   </webElementProperties>
</WebElementEntity>
