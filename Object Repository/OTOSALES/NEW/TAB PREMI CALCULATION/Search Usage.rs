<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Usage</name>
   <tag></tag>
   <elementGuidId>14143493-46b6-40ca-9ce7-98a4006ec93c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[contains(@placeholder,&quot;usage&quot;) and @name=&quot;search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[contains(@placeholder,&quot;usage&quot;) and @name=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
