<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button OK Pop UP</name>
   <tag></tag>
   <elementGuidId>ecd65c89-8220-48ac-8921-dbaf1587f405</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
   </webElementProperties>
</WebElementEntity>
