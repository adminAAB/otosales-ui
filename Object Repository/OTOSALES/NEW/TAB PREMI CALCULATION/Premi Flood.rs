<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Premi Flood</name>
   <tag></tag>
   <elementGuidId>7d53927c-5ad0-4bef-8973-c6f7564f3a90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;FLOOD&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;FLOOD&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
   </webElementProperties>
</WebElementEntity>
