<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Value Vehicle Type</name>
   <tag></tag>
   <elementGuidId>fba05c7b-4e9e-47e3-afdf-8f055f579ef6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @value=&quot;MULTY PURPOSE VEHICLE&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @value=&quot;MULTY PURPOSE VEHICLE&quot;]</value>
   </webElementProperties>
</WebElementEntity>
