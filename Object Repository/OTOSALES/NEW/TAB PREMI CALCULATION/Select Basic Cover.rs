<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Basic Cover</name>
   <tag></tag>
   <elementGuidId>22da9fa4-fcd8-4c88-a656-87d006892c07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;BASIC COVER * &quot;]//parent::div//input[@placeholder=&quot;Select basic cover&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;BASIC COVER * &quot;]//parent::div//input[@placeholder=&quot;Select basic cover&quot;]</value>
   </webElementProperties>
</WebElementEntity>
