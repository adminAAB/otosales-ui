<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Region</name>
   <tag></tag>
   <elementGuidId>401e9a9a-dab6-4955-867e-af17537d8c9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key3}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key3}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
