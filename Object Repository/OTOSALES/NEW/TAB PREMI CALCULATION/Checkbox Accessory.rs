<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Accessory</name>
   <tag></tag>
   <elementGuidId>21bc83b6-ec76-4296-a9cf-8c6c92a32379</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsACCESSChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsACCESSChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
