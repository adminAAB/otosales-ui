<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Product Code</name>
   <tag></tag>
   <elementGuidId>a4568078-80df-4778-9890-7f50d2026f35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder=&quot;Ketik Select product code&quot; and @name=&quot;search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder=&quot;Ketik Select product code&quot; and @name=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
