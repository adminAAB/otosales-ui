<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Sum Insured</name>
   <tag></tag>
   <elementGuidId>a302bb03-4fcd-4cfd-95cf-2808401cd4aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;SUM INSURED *&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;SUM INSURED *&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
