<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Product Code</name>
   <tag></tag>
   <elementGuidId>641dfd02-3106-4284-b298-0f44b9d88342</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key2}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key2}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
