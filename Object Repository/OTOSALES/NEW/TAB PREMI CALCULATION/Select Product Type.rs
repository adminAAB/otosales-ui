<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Product Type</name>
   <tag></tag>
   <elementGuidId>1d46809f-efc3-4a1c-8f34-aecf55536a0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;PRODUCT TYPE * &quot;]//parent::div//input[@placeholder=&quot;Select product type&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;PRODUCT TYPE * &quot;]//parent::div//input[@placeholder=&quot;Select product type&quot;]</value>
   </webElementProperties>
</WebElementEntity>
