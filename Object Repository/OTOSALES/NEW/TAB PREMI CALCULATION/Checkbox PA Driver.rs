<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox PA Driver</name>
   <tag></tag>
   <elementGuidId>f1a02c71-4622-4b8f-9648-66537ca36d38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsPADRVRChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsPADRVRChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
