<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Value Region</name>
   <tag></tag>
   <elementGuidId>0afd8806-0f6d-4223-b130-5a504fecef02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
   </webElementProperties>
</WebElementEntity>
