<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Chechbox Used Car</name>
   <tag></tag>
   <elementGuidId>2705e20d-e349-4720-8438-4485cb670ce7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div//input[@type=&quot;checkbox&quot; and @name=&quot;VehicleUsedCar&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div//input[@type=&quot;checkbox&quot; and @name=&quot;VehicleUsedCar&quot;]</value>
   </webElementProperties>
</WebElementEntity>
