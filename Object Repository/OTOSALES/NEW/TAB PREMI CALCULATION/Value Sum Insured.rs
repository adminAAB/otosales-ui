<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Value Sum Insured</name>
   <tag></tag>
   <elementGuidId>3eea1a17-b867-420e-babe-e4c5264fba13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>&lt;span>248,350,000&lt;/span></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>&lt;span>248,350,000&lt;/span></value>
   </webElementProperties>
</WebElementEntity>
