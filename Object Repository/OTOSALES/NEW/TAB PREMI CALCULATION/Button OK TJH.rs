<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button OK TJH</name>
   <tag></tag>
   <elementGuidId>b46f41bc-6d6b-4856-9532-990fd139df04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
   </webElementProperties>
</WebElementEntity>
