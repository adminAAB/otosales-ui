<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Premi EarthQuake</name>
   <tag></tag>
   <elementGuidId>39cc6ad9-f929-4fdc-af8b-b81d9f7df3c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;EARTHQUAKE&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;EARTHQUAKE&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
   </webElementProperties>
</WebElementEntity>
