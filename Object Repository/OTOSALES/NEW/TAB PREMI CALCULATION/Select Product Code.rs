<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Product Code</name>
   <tag></tag>
   <elementGuidId>16f37cdf-acf8-4f1f-944f-f796b072beaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;PRODUCT CODE * &quot;]//parent::div//input[@placeholder=&quot;Select product code&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;PRODUCT CODE * &quot;]//parent::div//input[@placeholder=&quot;Select product code&quot;]</value>
   </webElementProperties>
</WebElementEntity>
