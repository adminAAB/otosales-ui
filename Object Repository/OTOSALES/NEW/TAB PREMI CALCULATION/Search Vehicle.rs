<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Vehicle</name>
   <tag></tag>
   <elementGuidId>736372ab-5c55-4dae-91d1-97f79d5b3714</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Ketik Type Vehicle Details&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Ketik Type Vehicle Details&quot;]</value>
   </webElementProperties>
</WebElementEntity>
