<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox PA Passenger</name>
   <tag></tag>
   <elementGuidId>9debf49f-b1f0-4c08-8e01-c9996e36d6f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsPAPASSChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsPAPASSChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
