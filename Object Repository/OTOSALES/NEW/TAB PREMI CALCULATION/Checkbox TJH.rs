<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox TJH</name>
   <tag></tag>
   <elementGuidId>41cf671a-c260-4dac-b59a-b8e32250487a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsTPLChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsTPLChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
