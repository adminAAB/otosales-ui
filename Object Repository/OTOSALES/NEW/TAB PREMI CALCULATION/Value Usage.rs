<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Value Usage</name>
   <tag></tag>
   <elementGuidId>e1742c56-376b-4e96-861d-decacde1e155</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;USAGE * &quot;]//parent::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;USAGE * &quot;]//parent::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
