<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Region</name>
   <tag></tag>
   <elementGuidId>e8c0dbec-25c1-4250-b00c-62d191f04b14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;input-group&quot;]//input[@placeholder=&quot;Select region&quot;]</value>
   </webElementProperties>
</WebElementEntity>
