<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Vehicle Type</name>
   <tag></tag>
   <elementGuidId>014b91d5-7b98-46be-822c-cf89491a1137</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key1}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext&quot; and contains(text(),&quot;${key1}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
