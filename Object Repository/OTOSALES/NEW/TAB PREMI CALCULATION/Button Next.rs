<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Next</name>
   <tag></tag>
   <elementGuidId>7f0ac786-eff3-454d-99e1-99dc9412b8f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(text(),&quot;Next&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(),&quot;Next&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
