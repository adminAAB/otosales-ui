<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox MV Godig</name>
   <tag></tag>
   <elementGuidId>9ca6e7fe-1ff8-4eb2-93e0-6fa87e1c0ed7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='MV GO DIGITAL']//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='MV GO DIGITAL']//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
