<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button OK Pop up</name>
   <tag></tag>
   <elementGuidId>b8733718-38be-4a6e-80e5-f8ab2a51d170</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-footer&quot;]//button[text()=&quot;OK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-footer&quot;]//button[text()=&quot;OK&quot;]</value>
   </webElementProperties>
</WebElementEntity>
