<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Product Type</name>
   <tag></tag>
   <elementGuidId>2061203b-d0b9-4ee9-8456-82fa4800aa04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;PRODUCT TYPE * &quot;]//parent::div//input[@placeholder=&quot;Select product type&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;PRODUCT TYPE * &quot;]//parent::div//input[@placeholder=&quot;Select product type&quot;]</value>
   </webElementProperties>
</WebElementEntity>
