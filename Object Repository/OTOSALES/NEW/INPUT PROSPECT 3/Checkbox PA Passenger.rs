<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox PA Passenger</name>
   <tag></tag>
   <elementGuidId>e1ad7589-3aa5-4e18-a486-0ff2f6a47e8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsPAPASSChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsPAPASSChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
