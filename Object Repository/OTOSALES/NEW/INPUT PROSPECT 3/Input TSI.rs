<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input TSI</name>
   <tag></tag>
   <elementGuidId>d2194407-ad91-4492-9c50-b9438276896c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;SUM INSURED *&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;SUM INSURED *&quot;]//parent::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
