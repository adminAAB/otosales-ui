<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox SRCC</name>
   <tag></tag>
   <elementGuidId>d2bae35f-1b2f-4e0b-94c8-8b1544a00b53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsSRCCChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;checkbox&quot; and @name=&quot;IsSRCCChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
