<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Product Code</name>
   <tag></tag>
   <elementGuidId>d5b558bb-ce78-46f3-8748-9637a928180f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;PRODUCT CODE * &quot;]//parent::div//input[@placeholder=&quot;Select product code&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;PRODUCT CODE * &quot;]//parent::div//input[@placeholder=&quot;Select product code&quot;]</value>
   </webElementProperties>
</WebElementEntity>
