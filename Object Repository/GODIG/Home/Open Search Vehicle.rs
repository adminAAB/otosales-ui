<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open Search Vehicle</name>
   <tag></tag>
   <elementGuidId>7b54f422-2ceb-4322-828d-a5b027cae9c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@id=&quot;txtVehicle&quot;]//following-sibling::span//span[contains(text(),&quot;Toyota&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@id=&quot;txtVehicle&quot;]//following-sibling::span//span[contains(text(),&quot;Toyota&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
