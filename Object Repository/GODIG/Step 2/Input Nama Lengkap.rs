<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Nama Lengkap</name>
   <tag></tag>
   <elementGuidId>2f6a0a41-09f2-4c4d-aa48-acd56e0ba8bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//label[text()=&quot;NAMA LENGKAP *&quot;]//following-sibling::span//input[@id=&quot;NamaLengkap&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//label[text()=&quot;NAMA LENGKAP *&quot;]//following-sibling::span//input[@id=&quot;NamaLengkap&quot;])[1]</value>
   </webElementProperties>
</WebElementEntity>
