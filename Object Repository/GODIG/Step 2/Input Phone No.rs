<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Phone No</name>
   <tag></tag>
   <elementGuidId>85f8cd58-048d-4a46-a902-24fef6504a93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//input[@id=&quot;NoHandphone&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//input[@id=&quot;NoHandphone&quot;])[1]</value>
   </webElementProperties>
</WebElementEntity>
