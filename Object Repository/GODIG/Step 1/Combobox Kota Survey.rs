<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Kota Survey</name>
   <tag></tag>
   <elementGuidId>fe241bcb-ea5a-4e69-9f5d-8888e45e2964</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;KOTA SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyCity&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;KOTA SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyCity&quot;]</value>
   </webElementProperties>
</WebElementEntity>
