<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Warna Dasar</name>
   <tag></tag>
   <elementGuidId>4b2b25e0-8d88-4e5f-9961-9c36a3ffe436</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;WARNA DASAR KENDARAAN *&quot;]//following-sibling::span//select[@id=&quot;WarnaDasar&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;WARNA DASAR KENDARAAN *&quot;]//following-sibling::span//select[@id=&quot;WarnaDasar&quot;]</value>
   </webElementProperties>
</WebElementEntity>
