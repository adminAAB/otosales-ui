<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Pertanggunan - Option</name>
   <tag></tag>
   <elementGuidId>636bf91b-ff67-4e7f-b14b-4586996a9613</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@id=&quot;protectionType&quot;]//option[@value=&quot;${coverage}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@id=&quot;protectionType&quot;]//option[@value=&quot;${coverage}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
