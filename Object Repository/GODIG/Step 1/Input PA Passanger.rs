<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input PA Passanger</name>
   <tag></tag>
   <elementGuidId>9c0b5bd1-ebce-40e0-ab23-4a6a79207f14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;PERSONAL ACCIDENT PENUMPANG&quot;]//following-sibling::div//input[@id=&quot;paPassangerPrice&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;PERSONAL ACCIDENT PENUMPANG&quot;]//following-sibling::div//input[@id=&quot;paPassangerPrice&quot;]</value>
   </webElementProperties>
</WebElementEntity>
