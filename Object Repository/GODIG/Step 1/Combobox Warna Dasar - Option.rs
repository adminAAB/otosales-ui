<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Warna Dasar - Option</name>
   <tag></tag>
   <elementGuidId>c9b1661d-30d3-4789-87c1-e6fa444c4bda</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;WARNA DASAR KENDARAAN *&quot;]//following-sibling::span//select[@id=&quot;WarnaDasar&quot;]//option[text()=&quot;${colour}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;WARNA DASAR KENDARAAN *&quot;]//following-sibling::span//select[@id=&quot;WarnaDasar&quot;]//option[text()=&quot;${colour}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
