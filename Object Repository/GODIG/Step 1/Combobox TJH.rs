<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox TJH</name>
   <tag></tag>
   <elementGuidId>6675e8cb-21c0-4c25-8d53-b65c23cb6709</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;(TJH)&quot;)]//following-sibling::span//select[@id=&quot;thirdPartyPrice&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;(TJH)&quot;)]//following-sibling::span//select[@id=&quot;thirdPartyPrice&quot;]</value>
   </webElementProperties>
</WebElementEntity>
