<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Kota Survey - Option</name>
   <tag></tag>
   <elementGuidId>502b0659-0b5c-4f13-9d2e-ce7642640638</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;KOTA SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyCity&quot;]//option[text()=&quot;${city}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;KOTA SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyCity&quot;]//option[text()=&quot;${city}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
