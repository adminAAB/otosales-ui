<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open Survey Date</name>
   <tag></tag>
   <elementGuidId>3c7d523d-e2d0-4c79-a026-de17a472cd8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;TANGGAL SURVEI *&quot;]//following-sibling::span//input[@id=&quot;SurveyDateIn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;TANGGAL SURVEI *&quot;]//following-sibling::span//input[@id=&quot;SurveyDateIn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
