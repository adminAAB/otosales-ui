<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Search Result</name>
   <tag></tag>
   <elementGuidId>c21bf3e1-df45-4191-9650-b9cf82521221</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//table[contains(@class,&quot;a2is-datatable&quot;)]//tr[@data-bind])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//table[contains(@class,&quot;a2is-datatable&quot;)]//tr[@data-bind])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
