<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pop Up Existing Cust</name>
   <tag></tag>
   <elementGuidId>81a27b32-841b-451b-9f6c-20c0f59b1b3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),&quot;update existing&quot;)]//following-sibling::div//button[text()=&quot;Yes&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),&quot;update existing&quot;)]//following-sibling::div//button[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
