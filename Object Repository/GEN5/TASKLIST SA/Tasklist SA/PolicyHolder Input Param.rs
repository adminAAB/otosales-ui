<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PolicyHolder Input Param</name>
   <tag></tag>
   <elementGuidId>fb6849ea-b599-44b0-aeb1-4a8556434a6b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Search By&quot;])[3]//ancestor::div[3]/div//input[contains(@data-bind,&quot;textInput&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Search By&quot;])[3]//ancestor::div[3]/div//input[contains(@data-bind,&quot;textInput&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
