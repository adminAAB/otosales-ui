<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Next Object 2</name>
   <tag></tag>
   <elementGuidId>fc4b8811-acca-4060-9e23-5579d254241f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//h3[text()=&quot;Object&quot;]//parent::div//button[text()=&quot;Next&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//h3[text()=&quot;Object&quot;]//parent::div//button[text()=&quot;Next&quot;])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
