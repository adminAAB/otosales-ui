<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Radio Marketing</name>
   <tag></tag>
   <elementGuidId>dd608191-337a-4472-a496-69a84bd0de98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[contains(text(),&quot;Marketing&quot;)]//parent::span//parent::div//input[@type=&quot;radio&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[contains(text(),&quot;Marketing&quot;)]//parent::span//parent::div//input[@type=&quot;radio&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
