<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notifikasi Close Page</name>
   <tag></tag>
   <elementGuidId>7c8a0f4a-802c-45fb-9ef6-26b4453524bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(text(),&quot;Close application and go back to Menu page?&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(text(),&quot;Close application and go back to Menu page?&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
