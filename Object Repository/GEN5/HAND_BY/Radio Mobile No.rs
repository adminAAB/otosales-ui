<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Radio Mobile No</name>
   <tag></tag>
   <elementGuidId>f50e6fb5-3dd8-41d9-981e-2caee132c95b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[contains(text(),&quot;Mobile No&quot;)]//parent::span//parent::div//input[@type=&quot;radio&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[contains(text(),&quot;Mobile No&quot;)]//parent::span//parent::div//input[@type=&quot;radio&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
