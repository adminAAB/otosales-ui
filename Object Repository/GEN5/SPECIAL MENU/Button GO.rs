<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button GO</name>
   <tag></tag>
   <elementGuidId>fd5d5f44-a17c-4785-9001-512f0732b6b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGSpecialMenu-0&quot;]/div[1]/div/div[1]/div[3]/a2is-buttons/div/div/button[count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGSpecialMenu-0&quot;]/div[1]/div/div[1]/div[3]/a2is-buttons/div/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
