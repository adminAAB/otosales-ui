<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Pertanggungan - Option</name>
   <tag></tag>
   <elementGuidId>89d40157-f492-49a8-92ac-52bd2a1fa044</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Pertanggungan&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div//button[text()=&quot;${cover}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Pertanggungan&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div//button[text()=&quot;${cover}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
