<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Nama Tertanggung</name>
   <tag></tag>
   <elementGuidId>73fa33db-f00a-4f39-8331-d6322f9623ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Nama Tertanggung&quot;]//parent::label//following-sibling::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Nama Tertanggung&quot;]//parent::label//following-sibling::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
