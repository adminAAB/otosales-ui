<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox TJH - Option</name>
   <tag></tag>
   <elementGuidId>522e1b15-62dd-491e-bd9a-5cdda7745adf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Tanggung Jawab Hukum Pihak Ketiga&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div//button[contains(text(),&quot;${TJHAM}&quot;)][count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Tanggung Jawab Hukum Pihak Ketiga&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div//button[contains(text(),&quot;${TJHAM}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
