import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Define Nomor Rangka dan Nomor Mesin
CustomKeywords.'general.database.connectLiTT'()

def getChassisNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Chassis Number\'')

def getEngineNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Engine Number\'')

getChassisNo.next()

getEngineNo.next()

String ChassisNo = getChassisNo.getString('Value')

String EngineNo = getEngineNo.getString('Value')

def Rangka = CustomKeywords.'general.web.addChassisGodig'(Region, ChassisNo)

def Mesin = CustomKeywords.'general.web.addEngineGodig'(Region, EngineNo)

GlobalVariable.ChassisNo = Rangka

GlobalVariable.EngineNo = Mesin

String vehicleColor = Color.toUpperCase()

// Action Script 
WebUI.waitForElementClickable(findTestObject('Object Repository/GODIG/Step 1/Input No Rangka'), 3)

WebUI.waitForElementPresent(findTestObject('Object Repository/GODIG/Step 1/Input No Rangka'), 3)

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Pertanggungan'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Pertanggunan - Option',[('coverage') : Coverage ]))

WebUI.setText(findTestObject('Object Repository/GODIG/Step 1/Input No Rangka'), Rangka)

WebUI.setText(findTestObject('Object Repository/GODIG/Step 1/Input No Mesin'), Mesin)

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Warna Dasar'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Warna Dasar - Option',[('colour') : vehicleColor ]))

WebUI.setText(findTestObject('Object Repository/GODIG/Step 1/Input Warna STNK'), Color)

if (Driver) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Checkbox PA Driver'))
	
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 1/Input PA Driver'), DriverAmount)
}

if (Passenger) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Checkbox PA Passanger'))
	
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 1/Input PA Passanger'), PassAmount)
}

if (TJH) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Checkbox TJH'))
	
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox TJH'))
	
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Input TJH',[('TJHPrice') : TJHAmount ]))
}

if (SRCC) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Checkbox SRCC'))
}

if (Terrorisme) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Checkbox Terrorism'))
}

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Kota Survey'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Kota Survey - Option',[('city') : SurveyCity ]))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Location'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Location - Option'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Open Survey Date'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Datepicker Survey Date - Today'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Waktu Survey'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Combobox Waktu Survey - Option'))

// Update Value Chassis and Engine in database
CustomKeywords.'general.database.connectLiTT'()

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Chassis Number\'')

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Engine Number\'')

// Proceed to Next Step
WebUI.click(findTestObject('Object Repository/GODIG/Step 1/Button Next'))












