import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import org.apache.commons.io.FileUtils
import com.kms.katalon.core.configuration.RunConfiguration
//WebUI.openBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessURL("Otosales") -- BEFORE

File file = new File(RunConfiguration.getProjectDir() + "/Data Files/parameterLinkOtosales.txt");
String text = FileUtils.readFileToString(file)
//String text = '1'
 

switch (text) {
	case '1':
	 // GlobalVariable.URLType = '-dev-ext'
	WebUI.openBrowser(GlobalVariable.OtosalesQC)
	  break;
	case '2':
	 // GlobalVariable.URLType = '-qc'
	WebUI.openBrowser(GlobalVariable.OtosalesStaging)
	  break;
	
	default:
	  KeywordUtil.markErrorAndStop('Incorrect - Gagal Read File')
  }


  

/*if (GlobalVariable.URLType == '1')
{
	UI.AccessURL("Otosales")
	
}
else {
	
}
*/
WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

GlobalVariable.OtosalesLogin = username

WebUI.setText(findTestObject('Object Repository/OTOSALES/LOGIN/Input Username'), username, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OTOSALES/LOGIN/Input Password'), password, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/LOGIN/Button Login'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)