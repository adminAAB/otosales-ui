import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/APPROVAL/Cek Page Approval Next Ready'), GlobalVariable.LongestDelay)

WebUI.delay(3)

if (option == 'Approve') {
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Choose ApproveOrRevise' , [('choose') : option]))
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Button Approval - Approve', [('chooseAgain') : option]))
	
} else if (option == 'Revise'){
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Choose ApproveOrRevise' , [('choose') : option]))

	WebUI.setText(findTestObject('Object Repository/OTOSALES/APPROVAL/Input Remarks Revise'), Remarks)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Button Send Remarks'))
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Button Remarks Info Yes'))

} else if (option == 'Reject'){
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Choose ApproveOrRevise' , [('choose') : option]))
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Button Approval - Approve', [('chooseAgain') : option]))

} else {
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Choose ApproveOrRevise' , [('choose') : option]))
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/APPROVAL/Button Approval - Cancel'))
	
}
