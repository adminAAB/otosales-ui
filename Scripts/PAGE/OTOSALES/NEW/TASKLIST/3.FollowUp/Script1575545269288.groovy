import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.UI
import org.apache.commons.io.FileUtils
import com.kms.katalon.core.configuration.RunConfiguration
//WebUI.openBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessURL("Otosales") -- BEFORE

/*File file = new File(RunConfiguration.getProjectDir() + "/Data Files/parameterLinkOtosales.txt");
String text = FileUtils.readFileToString(file)
String IP 
 

switch (text) {
	case '1':
	 // GlobalVariable.URLType = '-dev-ext'
	IP = '172.16.94.48'
	  break;
	case '2':
	 // GlobalVariable.URLType = '-qc'
	IP = '172.16.94.48'
	  break;
	
	default:
	  KeywordUtil.markErrorAndStop('Incorrect - Gagal Read File')
  }
*/
// Wait for Page until ready to use
WebUI.delay(5)
WebUI.waitForPageLoad(GlobalVariable.LongestDelay)
WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)
//Cek role
String QuerySalesOfficer = 'SELECT * FROM dbo.SalesOfficer WHERE SalesOfficerID = \''+ GlobalVariable.OtosalesLogin +'\''
String UserRole = UI.getValueDatabase('172.16.94.48', 'AABMobile', QuerySalesOfficer, 'Role' )
// Preparation
if (UserRole == 'TELERENEWAL' || UserRole == 'TELESALES'){
boolean primary = WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Checkbox Primary CTI'), GlobalVariable.LongestDelay, FailureHandling.OPTIONAL)

// Action script
if (primary) {
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Checkbox Primary CTI'))
	
	boolean checkbox = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Checkbox Tertanggung CTI'), 3, FailureHandling.OPTIONAL)
	
	if (checkbox) {
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Checkbox Tertanggung CTI'))
	}
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Button Save CTI'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(2)
}


}

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Combobox Follow Up Reason'), FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Choose FU Reason', [('option') : Reason ]), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

if (Reason == 'Kirim Penawaran' || Reason == 'Kelengkapan Dokumen') {
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Combobox FU Notes'), FailureHandling.STOP_ON_FAILURE)	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Choose FU Notes', [('note') : Notes ]), FailureHandling.STOP_ON_FAILURE)
}

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Input Receiver'), Receiver, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Input Remarks'), Remarks, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Button Save'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Follow Up/Button OK'))
WebUI.delay(1)

//CustomKeywords.'general.scheduler.Otosales_CreateOrder'()

String Query = 'SELECT TOP 1* FROM dbo.ordersimulation WHERE FollowUpNo = (SELECT TOP 1 FollowUpNo FROM dbo.followup WHERE ProspectName = \'' + ProspectName + '\' ORDER BY EntryDate desc ) ORDER BY EntryDate DESC' 

String getOrderNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', Query, 'PolicyOrderNo')
String getPolicyNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', Query, 'PolicyNo')
String getOldPolicyNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', Query, 'OldPolicyNo')
String QueryMstOrder = 'SELECT * FROM mst_order WHERE Order_No = \'' + getOrderNo + '\''
String mstOrder = UI.getValueDatabase('172.16.94.74', 'AAB', QueryMstOrder, 'Order_No')
//String mstOrder = ''

while ((getOrderNo == 'null' || getOrderNo == '' || getOrderNo == null) && (mstOrder == 'null' || mstOrder == '' || mstOrder == null)) {
	KeywordUtil.markWarning("Retry run scheduler Create Order")
	//CustomKeywords.'general.scheduler.Otosales_CreateOrder'()
	
	println (getOrderNo)
	println (mstOrder)
	
	//String QueryMstOrder = "SELECT * FROM mst_order WHERE Order_No = '" + getOrderNo +"'"
	//String QueryMstOrder = 'SELECT * FROM mst_order WHERE Order_No = \'' + getOrderNo + '\''
	mstOrder = UI.getValueDatabase('172.16.94.74', 'AAB', QueryMstOrder, 'Order_No')
	getOrderNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', Query, 'PolicyOrderNo')
}

GlobalVariable.OtosalesPolicyNo = getPolicyNo
GlobalVariable.OtosalesOrderNo = getOrderNo
GlobalVariable.OtosalesOldPolicyNo = getOldPolicyNo

println (GlobalVariable.OtosalesOrderNo)

WebUI.closeBrowser()
