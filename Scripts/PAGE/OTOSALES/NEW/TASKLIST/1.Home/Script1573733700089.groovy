import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.UI


//Tambahan untuk Policy Created
String QueryOrderNo = 'select * from ordersimulationmv WHERE chasisNumber = \''+ Chassis +'\''
String OrderNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryOrderNo, 'OrderNo' )
String QueryPolicyNo = 'select * from ordersimulation WHERE OrderNo = \''+ OrderNo +'\''
String PolicyNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryPolicyNo, 'PolicyNo' )


WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Search'), 3)

WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Search'), 3)

WebUI.delay(1)
println (PolicyNo)
if (PolicyNo != 'null'){
	//Menampilkan Filter
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Filter'), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Filter - Checkbox Policy Created'), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Filter - Button Submit'), FailureHandling.STOP_ON_FAILURE)
    //end

	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Search'), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Combo Box Search'), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Combo Box Search Chasis'), FailureHandling.STOP_ON_FAILURE)

	WebUI.delay(1)

	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Field Search'), Chassis)

	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Field Search'), FailureHandling.STOP_ON_FAILURE)

	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Search Result by Name'), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

	String CustomerName = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Search Result by Name'), 
    FailureHandling.STOP_ON_FAILURE)
	String uppername = CustName.toUpperCase()
    String CustoName = uppername + ' - ' + PolicyNo 	
	println (CustomerName)
	println (CustoName)
	if (CustomerName == CustoName) {
		KeywordUtil.markPassed("Customer Name is Correct")
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Order'), FailureHandling.STOP_ON_FAILURE)
	} 
	else {
		
		KeywordUtil.markFailedAndStop("Hmmmm.. Whose name is that??")
	
}
}
else
{
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Search'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Combo Box Search'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Combo Box Search Chasis'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Field Search'), Chassis)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Button Field Search'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Search Result by Name'), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
	
	String CustomerName = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Search Result by Name'),
		FailureHandling.STOP_ON_FAILURE)
	
	if (CustomerName == CustName) {
		KeywordUtil.markPassed("Customer Name is Correct")
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Home/Choose Order'), FailureHandling.STOP_ON_FAILURE)
	} else {
	println(CustomerName)
		KeywordUtil.markFailedAndStop("Hmmmm.. Whose name is that??")
		
	}
}