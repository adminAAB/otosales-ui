import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

String[] upload = ['KTP','STNK','SPPAKB','BSTB'] // file which uploaded

String [] fileName = ['jackie chan.jpg','stephen chow.jpeg', 'Nicolas Cage.jpeg', 'sir john kotelawala.jpeg']

boolean MVGodig = WebUI.verifyElementNotChecked(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox MV Godig'), 2, FailureHandling.OPTIONAL)

boolean MVNonGodig = WebUI.verifyElementNotChecked(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox MV Non Godig'), 2, FailureHandling.OPTIONAL)

String QueryReg = 'SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\''

String RegistrationNo = UI.getValueDatabase('172.16.94.48','LiTT', QueryReg, 'Value')

GlobalVariable.ProspectName = prospectName + ' ' + RegistrationNo
GlobalVariable.CustPhone = phone1
GlobalVariable.type = isCompany
GlobalVariable.OrderType = MV
GlobalVariable.CashDealer = CashDealer

String uppersalesman = SalesNameFull.toUpperCase()


int i 

if (MV == 'MV GODIG') {
	if (MVGodig) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox MV Godig'))
	}
} else {
	if (MVNonGodig) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox MV Non Godig'))
	}
}

// choose company or personal customer
if(isCompany){
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox IsCompany'))
	
}

// fill customer's data fields
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Prospect Name'), GlobalVariable.ProspectName)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Phone 1'), phone1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Phone 2'), phone2)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Email'), email)

// This script will run if variable cash delaer is true
if (CashDealer) {
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Dealer Name'))
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Search Dealer Name'), DealerNameFull)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Dealer Name',[('key1') : DealerNameFull ]))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Input Salesman Dealer'))
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Search Salesman Dealer'), SalesNameFull)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Salesman Dealer', [('key2') : uppersalesman ]))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
}

// Upload File to Otosales
if(!isCompany) {
	for (i = 0 ; i < upload.size() ; i++) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Upload ' + upload[i] + ' image'), 20)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Upload ' + upload[i] + ' image'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
		
		WebUI.delay(5)
		
		UI.UploadFile2(fileName[i])
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(1)
	}
}

// Proceed to Next Step
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Button Next'))

// Handle phone number confirmation pop up
def ConfirmationPopup = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Pop up Confirmation'), 0, FailureHandling.STOP_ON_FAILURE)

if (ConfirmationPopup) {
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Conf Pop Up Yes'))

}

