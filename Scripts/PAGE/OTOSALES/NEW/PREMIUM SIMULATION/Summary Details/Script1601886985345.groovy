import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

String QueryReg = 'SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\''

String Test = UI.getValueDatabase('172.16.94.48', 'LiTT', QueryReg, 'Value')

String RegistrationNomor = UI.getValueDatabase('172.16.94.48', 'LiTT', 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\'', 
    'Value')
String ChassisNo = UI.getValueDatabase('172.16.94.48', 'LiTT', 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Chassis Number\'', 
    'Value')
String EngineNo = UI.getValueDatabase('172.16.94.48', 'LiTT', 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Engine Number\'', 
    'Value')

def Plate = CustomKeywords.'general.web.addRegistration'(GlobalVariable.Region, RegistrationNomor)

def Rangka = CustomKeywords.'general.web.addChassis'(GlobalVariable.Region, ChassisNo)

def Mesin = CustomKeywords.'general.web.addEngine'(GlobalVariable.Region, EngineNo)

GlobalVariable.type = isCompany

GlobalVariable.ProspectName = ((prospectName + ' ') + Test)

GlobalVariable.CustPhone = phone

GlobalVariable.RegistrationNo = Plate

GlobalVariable.ChassisNo = Rangka

GlobalVariable.EngineNo = Mesin

// choose company or personal customer
if (isCompany) {
    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Checkbox IsCompany'))
}

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input Prospect Name'), GlobalVariable.ProspectName)

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input PIC OR CUSTOMER PHONE NUMBER'), phone)

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input PIC OR CUSTOMER EMAIL ADDRES'), email)

WebUI.delay(1)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input Registration No'), Plate)
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input Chassis No'), Rangka)
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB SUMMARY DETAILS/Input Engine No'), Mesin)
WebUI.click(findTestObject('OTOSALES/NEW/TAB SUMMARY DETAILS/Button Save'))
WebUI.click(findTestObject('OTOSALES/NEW/TAB SUMMARY DETAILS/Button Save Order'))

UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Registration Number\'')
UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Chassis Number\'')
UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Engine Number\'')

