import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

boolean MVGodig = WebUI.verifyElementNotChecked(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox MV Godig'), 
    2, FailureHandling.OPTIONAL)

boolean MVNonGodig = WebUI.verifyElementNotChecked(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox MV Non Godig'), 
    2, FailureHandling.OPTIONAL)

GlobalVariable.OrderType = MV
GlobalVariable.Region = Region
String UpperVehicle = VehicleName.toUpperCase()
GlobalVariable.CashDealer = CashDealer

int i

if (MV == 'MV GODIG') {
    if (MVGodig) {
        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox MV Godig'))
    }
} else {
    if (MVNonGodig) {
        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox MV Non Godig'))
    }
}


WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Type Vehicle Details'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Search Vehicle'), UpperVehicle)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Vehicle Type', [
            ('key1') : UpperVehicle]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Vehicle Type', [('key1') : UpperVehicle]))

WebUI.delay(1)

/*WebUI.waitForElementAttributeValue(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Value Vehicle Type'), true)*/
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Product Type'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Product Type', [
            ('key1') : ProductType]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Product Type', [('key1') : ProductType]))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Product Code'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Search Product Code'), ProductCode)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button Search Product'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Product Code', [
            ('key2') : ProductCode]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Product Code', [('key2') : ProductCode]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Usage'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Search Usage'), Usage)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Usage', [('key2') : Usage]), 
    GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Usage', [('key2') : Usage]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Region'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Region', [('key3') : Region]), 
    GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Region', [('key3') : Region]))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

String[] separate = Coverage.split(' ')

int parseInt = CustomKeywords.'general.web.getInteger'(TJH)

int parseIntDR = CustomKeywords.'general.web.getInteger'(Driver)

int parseIntPas = CustomKeywords.'general.web.getInteger'(Passenger)

int parseAcc = Accessory.toInteger()

String attribute = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Sum Insured'), 
    'value')

// Start Action
WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Basic Cover'))

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Search Basic Cover'), Coverage)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Basic Cover', [('key3') : Coverage]), 
    GlobalVariable.MediumDelay)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Choose Basic Cover', [('key3') : Coverage]))

WebUI.delay(1)

//Set TJH
boolean CheckTJH = WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox TJH'), 
    GlobalVariable.LongestDelay, FailureHandling.OPTIONAL)

if ((!(CheckTJH) && ((separate[0]) != 'TLO')) && (ProductType != 'Lexus Insurance')) {
    KeywordUtil.markFailedAndStop('ERROR - Checkbox TJH is not Displayed') //Jika product IEP
} else {
    if (ProductType != 'Lexus Insurance') {
        if ((separate[0]) != 'TLO') {
            if (parseInt > 0) {
                WebUI.delay(1)

                WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox TJH'), 
                    GlobalVariable.MediumDelay)

                WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox TJH'))

              //  if ((ProductCode == 'GNAB0') || ((CashDealer == true) && (ProductType == 'Garda Oto'))) {
				if((ProductCode == 'GNAB0') ){
                    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), 
                        GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

                    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Open TJH'))

                    WebUI.delay(1)

                    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Open TJH'), TJH)

                    WebUI.delay(1)

                    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK TJH'))

                    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), 
                        GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
                } else {
                    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), 
                        GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

                    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Open TJH'))
					
                    WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select TJH', 
                            [('key4') : TJH]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

                    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select TJH', [('key4') : TJH]))

                    WebUI.delay(1)

                    WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK TJH'), 
                        10)

                    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK TJH'))

                    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), 
                        GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
                }
            }
        } else {
            println('Error - TJH must not displayed')
        }
    } else {
        println('Product Lexus Insurance do not have TJH')
    }
}

// Set SRCC - Flood - EarthQuake
if (((separate[0]) == 'Comprehensive') && (attribute < '200,000,000')) {
    WebUI.delay(1)

    WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox SRCC'), 
        GlobalVariable.MediumDelay)

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox SRCC'))

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))
	
    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
        FailureHandling.STOP_ON_FAILURE)
} else if (ProductCode == 'GNAB0') {
    WebUI.delay(1)

    WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox SRCC'), 
        GlobalVariable.MediumDelay)

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox SRCC'))

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

    WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
        FailureHandling.STOP_ON_FAILURE)
} else {
    def disable = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox SRCC - Disabled'), 
        10)

    if ((disable && ((separate[0]) == 'Comprehensive')) && (attribute > '200,000,000')) {
        println('Coverage is Comprehensive and TSI Above 200,000,000, therefor this menu is disable')
    } else if (disable && ((separate[0]) != 'Comprehensive')) {
        println('Coverage is TLO, therefor this menu is disable')
    } else if (!(disable)) {
        println('Error - This checkbox should be disable')
    } else {
        println('This checkbox disable because coverage contains TLO')
    }
}

// Set Terrorism
if (Terrorism) {
    if (ProductType != 'Lexus Insurance') {
        WebUI.delay(1)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox Terrorisme'), 
            GlobalVariable.MediumDelay)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox Terrorisme'))

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)

        String PremiTeror = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi Terrorism'), 
            FailureHandling.STOP_ON_FAILURE)

        GlobalVariable.PremiTerrorism = PremiTeror
    }
}

// Set PA Driver
if (parseIntDR > 0) {
    if (ProductType != 'Lexus Insurance') {
        WebUI.delay(1)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Driver'), 
            GlobalVariable.MediumDelay)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Driver'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select PA Driver'))

        WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input PA Driver', 
                [('key5') : Driver]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input PA Driver', [('key5') : Driver]))

        WebUI.delay(2)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'), 
            10)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)
    } else {
        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Driver'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select PA Driver'), Driver)

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)
    }
} else {
    println('PA Driver is not set')
}

// Set PA Passenger
if (parseIntPas > 0) {
    if (ProductType != 'Lexus Insurance') {
        WebUI.delay(1)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Passenger'), 
            GlobalVariable.MediumDelay)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Passenger'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select PA Passenger'))

        WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input PA Passenger', 
                [('key6') : Passenger]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input PA Passenger', [('key6') : Passenger]))

        WebUI.delay(2)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'), 
            10)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)
    } else {
        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox PA Passenger'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select PA Passenger'), Passenger)

        WebUI.delay(2)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)
    }
} else {
    println('PA Passenger is not set')
}

// Set Accessory
/*GlobalVariable.OrderType = MV*/
if (MV == 'MV NON GODIG') {
    if (parseAcc > 0) {
        WebUI.delay(2)

        WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox Accessory'), 
            GlobalVariable.MediumDelay)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Checkbox Accessory'))

        WebUI.delay(1)

        WebUI.clearText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input Accesory'), FailureHandling.STOP_ON_FAILURE)

        WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Input Accesory'), Accessory)

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button OK Pop UP'))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, 
            FailureHandling.STOP_ON_FAILURE)
    }
}

// Store Data

String Vehicle = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Type Vehicle Details'), 'value')
String Usage = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Value Usage'), 'value')
String Region = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Value Region'), 'value')

GlobalVariable.CustVehicle = Vehicle
GlobalVariable.CustUsage = Usage
GlobalVariable.CustRegion = Region

String ProductType = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Product Type'), 
    'value')

String ProductCode = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Product Code'), 
    'value')

String BasicCover = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Select Basic Cover'), 
    'value')

String PremiTJH = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi TJH'), FailureHandling.STOP_ON_FAILURE)

String PremiSRCC = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi SRCC'), FailureHandling.STOP_ON_FAILURE)

String PremiFlood = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi Flood'), FailureHandling.STOP_ON_FAILURE)

String PremiEarth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi EarthQuake'), 
    FailureHandling.STOP_ON_FAILURE)

String PremiDriver = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi PA Driver'), 
    FailureHandling.STOP_ON_FAILURE)

String PremiPassanger = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi PA Passanger'), 
    FailureHandling.STOP_ON_FAILURE)

String PremiAccessories = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Premi Accessories'), 
    FailureHandling.STOP_ON_FAILURE)

GlobalVariable.ProductType = ProductType

GlobalVariable.ProductCode = ProductCode

GlobalVariable.Coverage = BasicCover

GlobalVariable.PremiTJH = PremiTJH

GlobalVariable.PremiSRCC = PremiSRCC

GlobalVariable.PremiFlood = PremiFlood

GlobalVariable.PremiEarthquake = PremiEarth

//GlobalVariable.PremiTerrorism = PremiTeror

GlobalVariable.PremiDriver = PremiDriver

GlobalVariable.PremiPassanger = PremiPassanger

GlobalVariable.PremiAccessories = PremiAccessories

GlobalVariable.CashDealer = CashDealer
// Press Button Calculate Premi
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button Calculate Premi'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TAB PREMI CALCULATION/Button Next'))