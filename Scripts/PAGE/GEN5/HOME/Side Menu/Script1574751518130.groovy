import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

boolean exist = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HOME/Collapse',[('menus') : SideMenu ]), 3, FailureHandling.OPTIONAL)
//confirm to record
boolean cctv = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HOME/Confirm to record'),3, FailureHandling.OPTIONAL)
if (cctv == true){
WebUI.click(findTestObject('Object Repository/GEN5/HOME/Confirm to record'))
}

if (SideMenu == 'Special Menu')
{
	WebUI.click(findTestObject('Object Repository/GEN5/HOME/Menu Parent - Special Menu',[('menus') : SideMenu ]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/GEN5/HOME/Collapse',[('menus') : SideMenu ]), FailureHandling.STOP_ON_FAILURE)
	
}
else
{
while (exist == false) {
	WebUI.click(findTestObject('Object Repository/GEN5/HOME/Menu Parent',[('menus') : SideMenu ]), FailureHandling.CONTINUE_ON_FAILURE)
	
	exist = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HOME/Collapse',[('menus') : SideMenu ]), 3)
}
WebUI.click(findTestObject('Object Repository/GEN5/HOME/Side Menu',[('menus') : SideMenu ]), FailureHandling.STOP_ON_FAILURE)

}

WebUI.delay(2)
