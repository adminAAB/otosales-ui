import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import org.apache.commons.io.FileUtils
import com.kms.katalon.core.configuration.RunConfiguration
//WebUI.openBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessBrowser(GlobalVariable.URLOtosalesCentos)

//UI.AccessURL("Otosales") -- BEFORE

File file = new File(RunConfiguration.getProjectDir() + "/Data Files/parameterLinkOtosales.txt");
String text = FileUtils.readFileToString(file)
//String text = '1'
 

switch (text) {
	case '1':
	 // QC
	WebUI.openBrowser(GlobalVariable.Gen5QC)
	  break;
	case '2':
	 // Staging
	WebUI.openBrowser(GlobalVariable.Gen5Staging)
	  break;
	
	default:
	  KeywordUtil.markErrorAndStop('Incorrect - Gagal Read File')
  }

//WebUI.openBrowser('https://172.16.94.5/retail')
//GEN5.AccessURL("Gen5")

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/GEN5/LOGIN/Input Username'), username, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/LOGIN/Input Password'), password, FailureHandling.STOP_ON_FAILURE)

if(isSyariah){
	
	//WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Checkbox IsCompany'))
	WebUI.click(findTestObject('Object Repository/GEN5/LOGIN/Type_Login'))
	WebUI.click(findTestObject('Object Repository/GEN5/LOGIN/Type_Login_Syariah'))
}

WebUI.click(findTestObject('Object Repository/GEN5/LOGIN/Button Masuk'), FailureHandling.STOP_ON_FAILURE)