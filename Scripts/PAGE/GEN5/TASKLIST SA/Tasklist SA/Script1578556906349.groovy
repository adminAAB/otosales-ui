import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.ArrayList
import org.openqa.selenium.By as By

// Remarks Section
WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Remarks'), 5)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Remarks'))

WebUI.waitForElementVisible(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/text customer type'), 30)

WebUI.delay(2)

// Personal Customer
if (GlobalVariable.type == false) {
	for (i = 0 ; i < 3 ; i++) {
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Personal Customer'))
		GEN5.ProcessingCommand()
	}
	
} else {
	for (i = 0 ; i < 3 ; i++) {
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Company Customer'))
		GEN5.ProcessingCommand()
	}
}
//WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Cust'), 3 , FailureHandling.CONTINUE_ON_FAILURE)

boolean Existing = WebUI.waitForElementVisible(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Cust'), 2, FailureHandling.OPTIONAL)

if (Existing) {
	WebUI.waitForElementClickable(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Cust'), 5)
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Cust'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Updated OK'))
	
	GEN5.ProcessingCommand()

} else {
	WebUI.waitForElementClickable(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Cust Validation'), GlobalVariable.LongestDelay)

	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Cust Validation'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Cust'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Existing Updated OK'))
	
	WebUI.delay(2)
}

if (changePolicyHolder){
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Open PolicyHolder'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Open PolicyHolder Param'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/PolicyHolder Search Param', [('param') : SearchParam ] ))
	
	WebUI.setText(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/PolicyHolder Input Param'), PolicyHolderName )
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Policy Holder Search'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Policy Holder'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Policy Holder Select'))
}

String PolicyName = WebUI.getText(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Name on Policy'), FailureHandling.STOP_ON_FAILURE)

//println(PolicyName)

if (PolicyName == GlobalVariable.ProspectName) {
	println('Passed - Name on Policy Same with Prospect Name')
} else {
	println('Failure - Name on Policy different with Prospect Name')
}

if (NameOnPolicy == 'Default') {
	println('Name on Policy is ' + GlobalVariable.ProspectName)
	
} else if (NameOnPolicy == 'New') {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Name on Policy'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Name On Policy 2'))
	
} else if (NameOnPolicy == 'DefaultQQNew') {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Name on Policy'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Name On Policy 3'))

} else if (NameOnPolicy == 'NewQQDefault') {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Name on Policy'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Name On Policy 4'))
	
} else if (NameOnPolicy == 'Other') {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Name on Policy'))
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Name on Policy Other'))
	
}
//update terkait dealer i<2
for (i = 0 ; i < 2 ; i++) {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Policy'))	
	GEN5.ProcessingCommand()
}
/*
if (CashDealer) {
	boolean Dealer = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.MediumDelay)
	if (Dealer){
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Policy'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
			10, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.delay(2)
	}
	else {
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Search Dealer'))
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/DropDown Search Dealer'))
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose DropDown Search Dealer'))
		WebUI.setText(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Textbox Input Dealer'))
		WebUI.click(findTestObject('GEN5/TASKLIST SA/Tasklist SA/Button Search Choose Dealer'))
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Dealer'))
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Select Dealer'))
		
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Policy'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
			10, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.delay(2)
	}
}
*/
// update terkait dealer end
WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Combobox Email Deliver to'))

WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Email Delivery', [('email'): EmailDelivery ] ))

WebUI.delay(2)

for (i = 0 ; i < 2 ; i++) {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Policy'))	
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Confirmation Update'))

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Information'), 20)

WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Information'))

WebUI.delay(2)

// Object Section
if (editVehicle) {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Vehicle'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Edit Vehicle'))
	
	boolean information = WebUI.waitForElementVisible(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/BTN Close Information'), 2, FailureHandling.OPTIONAL)
	
	while (information) {
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/BTN Close Information'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Choose Vehicle'))
		
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Edit Vehicle'))
	}
	
	GEN5.ProcessingCommand()

	for (i = 0 ; i < 2 ; i++) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Object 2'), GlobalVariable.DefaultDelay)
		
		WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Object 2'))
		
		GEN5.ProcessingCommand()
	}

	String Gross = GlobalVariable.OtosalesGrossPremi.replace(',','')
	String GrossPremi = Gross.replace('Rp.','')
	
	ArrayList getData = GEN5.getAllColumnValue(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Table Sum Insured'), 'Premium')
	
	for (int i = 0 ; i < getData.size() ; i++) {
		String change = getData[i].replace(',00','').replace('.','')
		getData.set(i, change)
	}
	
	//if (i == 4) {
	//	int vehicleInt = getData[0].toInteger()
	//	int accInt = getData[1].toInteger()
	//	int driverInt = getData[2].toInteger()
	//	int PassangerInt = getData[3].toInteger()
	//	int TJHInt = getData[4].toInteger()
	//int calcPremi = vehicleInt + accInt + driverInt + PassangerInt + TJHInt
		
	//String fixPremi = calcPremi.toString()
	//}
	//else {
	int vehicleInt = getData[0].toInteger()
	int accInt = getData[1].toInteger()
	int driverInt = getData[2].toInteger()
	int PassangerInt = getData[3].toInteger()
	int TJHInt = getData[5].toInteger()
	int calcPremi = vehicleInt + accInt + driverInt + PassangerInt + TJHInt
	
	String fixPremi = calcPremi.toString()
	
	//}
	
	if (fixPremi == GrossPremi) {
		println('Premi Vehicle is Correct')
	} else {
		println('FAILED - Premi Vehicle is Incorrect')
	}
	
	if (getData[1] == GlobalVariable.PremiAccessories.replace(',','')) {
		println('Premi Accessories is Correct')
	} else {
		println('FAILED - Premi Accessories is Incorrect or Order has Bundling (SRCC or Earthquake or Flood)')
	}
	
	if (getData[2] == GlobalVariable.PremiDriver.replace(',','')) {
		println('Premi PA Driver is Correct')
	} else {
		println('FAILED - Premi PA Driver is Incorrect')
	}
	
	if (getData[3] == GlobalVariable.PremiPassanger.replace(',','')) {
		println('Premi PA Passanger is Correct')
	} else {
		println('FAILED - Premi PA Passanger is Incorrect')
	}
	
	if (getData[5] == GlobalVariable.PremiTJH.replace(',','')) {
		println('Premi TJH is Correct')
	} else {
		println('FAILED - Premi TJH is Incorrect or Coverage is TLO')
	}
	
	WebUI.switchToDefaultContent()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Save'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Confirmation Save'))
	
	WebUI.delay(5)
	
	GEN5.ProcessingCommand()
} 
	
WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Next Object'))

GEN5.ProcessingCommand()

//Order Summary
if (Choice == 'Submit' ) {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Submit', [('continue') : Choice ]))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Confirmation Insert Yes'))
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Information'), 20, FailureHandling.STOP_ON_FAILURE)
	/*
	String getPolicyNo = WebUI.getText(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/TXT PolicyNo'))
	String[] separate = getPolicyNo.split(' : ')
	
	String PolicyNo = separate[1].trim()
	
	if (PolicyNo == GlobalVariable.OtosalesPolicyNo) {
		KeywordUtil.markPassed("Policy No is Correct")
	} else {
		KeywordUtil.markFailed("Tasklist SA GEN5 has different Policy No")
	}
	*/
	
} else if (Choice == 'Send Back to AO' ) {
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Button Submit', [('continue') : Choice ]))
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Input Remarks'), Remarks)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/GEN5/TASKLIST SA/Tasklist SA/Pop Up Information'))
}

WebUI.delay(2)

//CustomKeywords.'general.scheduler.Otosales_Approval_New'()

WebUI.delay(2)

//CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()

WebUI.delay(2)

String QueryAABMobile = 'SELECT FollowUpInfo,* FROM followUp WHERE FollowUpNo = (SELECT followUpNo FROM OrderSimulation WHERE PolicyOrderNo = \''+ GlobalVariable.OtosalesOrderNo +'\')'
String description = GEN5.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABMobile, 'FollowUpInfo')

String QueryAAB = 'SELECT TOP 1* FROM mst_order_mobile_approval where Order_no = \''+ GlobalVariable.OtosalesOrderNo +'\' and ApprovalStatus = \'0\' order by ApprovalType ASC'
String exist = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryAAB, 'ApprovalType')
String QueryOrderStatus = 'select * from mst_order WHERE Order_No = \''+ GlobalVariable.OtosalesOrderNo +'\''
String OrderStatus = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryOrderStatus, 'Order_Status' )

println(exist)
println(OrderStatus)
//while ((exist == null && OrderStatus != '9')|| description != '62' ) {
while (exist == 'null' && OrderStatus != '9' ) {
	    //CustomKeywords.'general.scheduler.Otosales_Approval_New'()
		WebUI.delay(2)
		//CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()
		
		//description = GEN5.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABMobile, 'FollowUpInfo')
		exist = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryAAB, 'ApprovalType')
		OrderStatus = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryOrderStatus, 'Order_Status' )
}

println(exist)
println(OrderStatus)
WebUI.closeBrowser()



