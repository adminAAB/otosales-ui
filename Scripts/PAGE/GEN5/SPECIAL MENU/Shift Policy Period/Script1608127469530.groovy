import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

GlobalVariable.SpecialMenu = ProcessName
GlobalVariable.PolicyNo = PolicyNumber

WebUI.click(findTestObject('Object Repository/GEN5/SPECIAL MENU/TextArea Process Name'))

WebUI.setText(findTestObject('Object Repository/GEN5/SPECIAL MENU/Input Process Name'), ProcessName)
WebUI.click(findTestObject('Object Repository/GEN5/SPECIAL MENU/Choose Process Name'))
WebUI.click(findTestObject('Object Repository/GEN5/SPECIAL MENU/Button GO'))
WebUI.setText(findTestObject('Object Repository/GEN5/SPECIAL MENU/Input Policy Number'), PolicyNumber)
WebUI.click(findTestObject('Object Repository/GEN5/SPECIAL MENU/Input Period'))

GEN5.DatePicker("17/Dec/2019",findTestObject('Object Repository/GEN5/SPECIAL MENU/Input Date'),true)

WebUI.click(findTestObject('Object Repository/GEN5/SPECIAL MENU/Button Execute'))
WebUI.delay(2)
/*
String QueryAAB = 'SELECT TOP 1* FROM mst_order_mobile_approval where Order_no = \''+ GlobalVariable.OtosalesOrderNo +'\' and ApprovalStatus = \'0\' order by ApprovalType ASC'
String exist = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryAAB, 'ApprovalType')
String QueryOrderStatus = 'select * from mst_order WHERE Order_No = \''+ GlobalVariable.OtosalesOrderNo +'\''
String OrderStatus = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryOrderStatus, 'Order_Status' )




//while ((exist == null && OrderStatus != '9')|| description != '62' ) {
while (exist == null && OrderStatus != '9' ) {
		CustomKeywords.'general.scheduler.Otosales_Approval_New'()
		WebUI.delay(2)
		CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()
		
		description = GEN5.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABMobile, 'FollowUpInfo')
		exist = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryAAB, 'ApprovalType')
		
	
}*/