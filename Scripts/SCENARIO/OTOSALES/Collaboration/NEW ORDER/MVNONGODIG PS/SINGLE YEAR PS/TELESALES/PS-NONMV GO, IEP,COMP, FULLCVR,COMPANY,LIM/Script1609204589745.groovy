import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
WebUI.callTestCase(findTestCase('SCENARIO/OTOSALES/LOGIN/Login Telesales'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'), [('menu') : 'Premium Simulation'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/PREMIUM SIMULATION/Premi Calculation'), [('MV') : 'MV NON GODIG', ('VehicleName') : 'MERCEDES BENZ C-CLASS 2018 C 200 EDITION C A/T'
        , ('ProductType') : 'Garda Oto', ('ProductCode') : 'GWU50', ('Usage') : 'PRIBADI', ('Region') : 'Wilayah 2', ('Coverage') : 'Comprehensive 1 Year'
        , ('CashDealer') : false, ('TJH') : '25,000,000', ('Terrorism') : true, ('Driver') : '20,000,000', ('Passenger') : '10,000,000'
        , ('Accessory') : '7000000'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/PREMIUM SIMULATION/Summary Details'), [('isCompany') : true, ('prospectName') : 'PT Auto Scrum Otosales'
        , ('phone') : '085373039414', ('email') : 'ehu@beyond.asuransiastra.com'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'), [('menu') : 'Task List' // Choose Side Menu
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/1.Home'), [('Chassis') : GlobalVariable.ChassisNo // Take Chassis Number from Global Variable Automatically
        , ('CustName') : GlobalVariable.ProspectName // Take Customer Name from Global Variable Automatically
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/2.Tasklist'), [('birthDate') : '20/10/1993', ('NomorNPWP') : '371829374123'
        , ('JenisKelamin') : 'Laki-Laki', ('Alamat') : 'Jalan Automate Katalon Scrum Otosales No Sprint 15', ('KodePos') : '55141'
        , ('PICName') : 'Auto PIC Name', ('Identitas') : '3471081510890001', ('docRep') : false // true if check docRep's checkbox and false to let it be
        , ('DocRepAmount') : '2000000', ('uploadDocRep') : false // true to upload document Rep
        , ('payerCompany') : false // true to check Payer company checkbox
        , ('changePeriod') : false // true to change policy period from
        , ('periodFrom') : '30/12/2018' // Change policy period from with slash ("/") separator
        , ('WarnaKendaraan') : 'HITAM', ('Paid') : false // true to upload Bukti Bayar
        , ('SegmentCode') : 'P3C100', ('SentTo') : 'Customer' /*('AlamatGC') : 'Mall Cipinang Indah', ('KodePosOther') : '17421'*/ 
        , ('survey') : false // true if want Activate checkbox survey
        , ('NeedNSA') : false // True to check NSA Skip Survey
        , ('SurveyAdd') : false // true to check Survey Additional Order
        , ('kotaSurvey') : 'Bekasi', ('NSAApproval') : false // True to upload NSA Document
        , ('RemarkToSA') : 'Auto Remarks by Katalon Studio' // Remarks to SA
        , ('Status') : 'Potential', ('Terrorism') : true], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/3.FollowUp'), [('Reason') : 'Send to SA' // Choose Send to SA, Kirim Penawaran or Kelengkapan Dokumen
        , ('Notes') : 'Contacted' // Choose Contacted, Not Connected or Not Contacted
        , ('Receiver') : 'Sir Cok Kotelawala' // Write Follow Up person's name
        , ('Remarks') : 'Created by Katalon Studio' // Free text
        , ('ProspectName') : GlobalVariable.ProspectName], FailureHandling.STOP_ON_FAILURE)

