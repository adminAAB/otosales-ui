import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('SCENARIO/OTOSALES/LOGIN/Login Telerenewal'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'), [('menu') : 'Input Prospect' // Write the side menu
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/1.Prospect Info'), [('MV') : 'MV NON GODIG' // Option MV GODIG or MV NON GODIG
        , ('isCompany') : false // True if the order is company, false to personal
        , ('prospectName') : 'Automate Customer Non Godig', ('phone1') : '082127670262', ('phone2') : '081290154328', ('email') : 'cok@cak.ep'
        , ('CashDealer') : false // true if the order is Cash Dealer, false to otherwise
        , ('DealerNameFull') : 'AUTO 2000 CILANDAK JKT', ('SalesNameFull') : 'CANDRA NUGROHO'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/2.Vehicle Info'), [('VehicleName') : 'MERCEDES BENZ C-CLASS 2018 C 200 EDITION C A/T' // Write the vehicle name and year
        , ('Usage') : 'PRIBADI' // Vehicle usage
        , ('Region') : 'Wilayah 2' // Vehicle area
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/3.Cover'), [('ProductType') : 'Garda Oto' // Choose Garda Oto, Garda Oto Syariah, Toyota Insurance or Lexus Insurance
        , ('MV') : GlobalVariable.OrderType, ('ProductCode') : 'GNAB0' // Choose Product Code base on Product Type
        , ('Coverage') : 'Comprehensive 3 Years' // Choose Basic Coverage
        , ('TJH') : '25,000,000' // TJH Amount 0 for No TJH and higher than 0 to choose TJH (coma separator)
        , ('Terrorism') : false // True to Choose Terrorism and false to skip it
        , ('Driver') : '20,000,000' // Driver Amount 0 for No PA Driver & higher than 0 to otherwise (coma separator)
        , ('Passenger') : '10,000,000' // Passanger Amount 0 for No PA Pass & higher than 0 to otherwise (coma separator)
        , ('Accessory') : '7000000' // Accessories Amount 0 for No Acc & higher than 0 to otherwise
        , // Accessories Amount 0 for No Acc & higher than 0 to otherwise
        ('CashDealer') : false], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'), [('menu') : 'Task List' // Choose Side Menu
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/1.Home'), [('Chassis') : GlobalVariable.ChassisNo // Take Chassis Number from Global Variable Automatically
        , ('CustName') : GlobalVariable.ProspectName // Take Customer Name from Global Variable Automatically
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/2.Tasklist'), [('birthDate') : '15/10/1989', ('NomorNPWP') : '371829374123'
        , ('JenisKelamin') : 'Laki-Laki', ('Alamat') : 'Jalan Pelan - Pelan', ('KodePos') : '55141', ('PICName') : 'Auto PIC Name'
        , ('Identitas') : '3471081510890001', ('docRep') : false // true if check docRep's checkbox and false to let it be
        , ('DocRepAmount') : '2000000', ('uploadDocRep') : false // true to upload document Rep
        , ('payerCompany') : false // true to check Payer company checkbox
        , ('changePeriod') : false // true to change policy period from
        , ('periodFrom') : '30/12/2018' // Change policy period from with slash ("/") separator
        , ('WarnaKendaraan') : 'HITAM', ('Paid') : false // true to upload Bukti Bayar
        , ('SegmentCode') : 'P3A600', ('SentTo') : 'Branch', ('AlamatGC') : 'Mall Cipinang Indah', ('KodePosOther') : '17421'
        , ('survey') : false // true if want Activate checkbox survey
        , ('NeedNSA') : false // True to check NSA Skip Survey
        , ('SurveyAdd') : false // true to check Survey Additional Order
        , ('kotaSurvey') : 'Bekasi', ('NSAApproval') : false // True to upload NSA Document
        , ('RemarkToSA') : 'Auto Remarks by Katalon Studio' // Remarks to SA
        , ('Status') : 'Potential', ('Terrorism') : false], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/3.FollowUp'), [('Reason') : 'Send to SA' // Choose Send to SA, Kirim Penawaran or Kelengkapan Dokumen
        , ('Notes') : 'Contacted' // Choose Contacted, Not Connected or Not Contacted
        , ('Receiver') : 'Sir Cok Kotelawala' // Write Follow Up person's name
        , ('Remarks') : 'Created by Katalon Studio' // Free text
        , ('ProspectName') : GlobalVariable.ProspectName], FailureHandling.STOP_ON_FAILURE)

