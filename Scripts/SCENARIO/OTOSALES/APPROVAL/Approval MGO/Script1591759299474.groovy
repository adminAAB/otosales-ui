import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


String QuerySalesOfficer = 'SELECT TOP 1* FROM dbo.SalesOfficer WHERE email = \''+ GlobalVariable.OtosalesLogin +'\''
String UserID = UI.getValueDatabase('172.16.94.48', 'AABMobile', QuerySalesOfficer, 'SalesOfficerID')
//String QueryAABHead = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ UserID +'\' and RowStatus = \'1\''
String QueryAABHead = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ UserID +'\' and RowStatus = \'1\' and Value = \'SRW\'' 
String Login = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABHead, 'Value')


String Approve = 'SELECT TOP 1* FROM mst_order_mobile_approval where Order_no = \''+ GlobalVariable.OtosalesOrderNo +'\' and ApprovalStatus = \'0\' order by ApprovalType ASC'
String approvalType = UI.getValueDatabase('172.16.94.74', 'AAB', Approve, 'ApprovalType')

String QueryMstOrder = 'SELECT * FROM mst_order WHERE Order_No = \'' + GlobalVariable.OtosalesOrderNo + '\''
String OrdStatus = UI.getValueDatabase('172.16.94.74', 'AAB', QueryMstOrder, 'Order_Status')
//Cek Approval Link Payment
String OrderNo = 'SELECT * FROM dbo.OrderSimulation WHERE PolicyOrderno = \''+ GlobalVariable.OtosalesOrderNo +'\''
String OrderNoLong = UI.getValueDatabase('172.16.94.48', 'AABMOBILE', OrderNo, 'OrderNo')
String ApproveLP = 'SELECT * FROM dbo.OrderSimulationApproval where OrderNo = \''+ OrderNoLong +'\' and RowStatus = \'1\' and ApprovalStatus = \'0\''
String approvalTypeLP = UI.getValueDatabase('172.16.94.48', 'AABMOBILE', ApproveLP, 'ApprovalStatus')

while (approvalType != 'null' && OrdStatus != '9') { 

	if (approvalType == 'COMSAMD' || approvalType == 'KOMISI') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): Login ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
		
	} else if (approvalType == 'ADJUST') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): Login ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
	} else {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): 'MWU' ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
	}
	
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
		[('menu') : 'Order Approval'], 								// Write the side menu
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval'),
		[('Name') : GlobalVariable.ProspectName ], 					
		FailureHandling.STOP_ON_FAILURE)
	
	if (approvalType == 'COMSAMD' || approvalType == 'KOMISI ') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
			[('option') : 'Approve' , 								// Choose Approve or Revise
			('Remarks') : 'Automate Remarks to Revise' ],
			FailureHandling.STOP_ON_FAILURE)
	
		} else if (approvalType == 'ADJUST ') {
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
				[('option') : 'Approve' , 							// Choose Approve or Revise
				('Remarks') : 'Automate Remarks to Revise'],
				FailureHandling.STOP_ON_FAILURE)
			
		} else {
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
				[('option') : 'Approve' , 							// Choose Approve or Reject
				('Remarks') : 'Automate Remarks to Revise' ],
				FailureHandling.STOP_ON_FAILURE)
		}
		
	
		
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
		[('menu') : 'Logout'], 								// Write the side menu
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(2)
	//CustomKeywords.'general.scheduler.Otosales_Approval_Gen5'()
	WebUI.delay(2)
	//CustomKeywords.'general.scheduler.Otosales_Approval_New'()
	WebUI.delay(2)
	//CustomKeywords.'general.scheduler.Otosales_Approval_Next_Limit'()
	OrdStatus = UI.getValueDatabase('172.16.94.74','AAB', QueryMstOrder, 'Order_Status')
	approvalType = UI.getValueDatabase('172.16.94.74', 'AAB', Approve, 'ApprovalType')
}

//Cek approval link Payment
println (approvalTypeLP)
	if (approvalTypeLP == '0') {
	
		
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
				[('username'): Login ,
				('password'): 'ITG@nt1P455QC'],
				FailureHandling.STOP_ON_FAILURE)
			
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
			[('menu') : 'Order Approval'], 								// Write the side menu
			FailureHandling.STOP_ON_FAILURE)
		
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval'),
			[('Name') : GlobalVariable.ProspectName ],
			FailureHandling.STOP_ON_FAILURE)
		
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
				[('option') : 'Approve' , 								// Choose Approve or Revise
				('Remarks') : 'Automate Remarks to Revise' ],
				FailureHandling.STOP_ON_FAILURE)
		
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
			[('menu') : 'Logout'], 								// Write the side menu
			FailureHandling.STOP_ON_FAILURE)
		
	
	}

WebUI.delay(2)
//CustomKeywords.'general.scheduler.Otosales_Approval_Gen5'()
WebUI.delay(2)
//CustomKeywords.'general.scheduler.Otosales_Approval_New'()
WebUI.delay(2)
//CustomKeywords.'general.scheduler.Otosales_Approval_Next_Limit'()
WebUI.delay(2)
//CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()
		
String QueryOrderStatus = 'select * from mst_order WHERE Order_No = \''+ GlobalVariable.OtosalesOrderNo +'\''
String OrderStatus = UI.getValueDatabase('172.16.94.74', 'AAB', QueryOrderStatus, 'Order_Status' )
String QueryAABMobile = 'SELECT FollowUpInfo,* FROM followUp WHERE FollowUpNo = (SELECT followUpNo FROM OrderSimulation WHERE PolicyOrderNo = \''+ GlobalVariable.OtosalesOrderNo +'\')'
String description = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABMobile, 'FollowUpInfo')


//while ((exist == null && OrderStatus != '9')|| description != '62' ) {
while (description != '50' ) {
		//CustomKeywords.'general.scheduler.Otosales_Approval_New'()
		WebUI.delay(2)
		//CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()
		
		description = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABMobile, 'FollowUpInfo')
		//exist = GEN5.getValueDatabase('172.16.94.74', 'AAB', QueryAAB, 'ApprovalType')
		OrderStatus = UI.getValueDatabase('172.16.94.74', 'AAB', QueryOrderStatus, 'Order_Status' )
}

println(description)
WebUI.closeBrowser()