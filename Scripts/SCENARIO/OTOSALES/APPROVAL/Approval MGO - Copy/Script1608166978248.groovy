import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


String QuerySalesOfficer = 'SELECT TOP 1* FROM dbo.SalesOfficer WHERE email = \''+ GlobalVariable.OtosalesLogin +'\''
String UserID = UI.getValueDatabase('172.16.94.48', 'AABMobile', QuerySalesOfficer, 'SalesOfficerID')
//String QueryAABHead = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ UserID +'\' and RowStatus = \'1\''
String QueryAABHead = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ UserID +'\' and RowStatus = \'1\' and Value = \'SRW\'' 
String Login = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABHead, 'Value')


String Approve = 'SELECT TOP 1* FROM mst_order_mobile_approval where Order_no = \''+ GlobalVariable.OtosalesOrderNo +'\' and ApprovalStatus = \'0\' order by ApprovalType ASC'
String approvalType = UI.getValueDatabase('172.16.94.74', 'AAB', Approve, 'ApprovalType')

while (approvalType != 'null') { 
	if (approvalType == 'COMSAMD' || approvalType == 'KOMISI') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): GlobalVariable.OtosalesLogin ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
		
	} else if (approvalType == 'ADJUST') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): Login ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
	} else {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Login'),
			[('username'): 'MWU' ,
			('password'): 'ITG@nt1P455QC'],
			FailureHandling.STOP_ON_FAILURE)
	}
	
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
		[('menu') : 'Order Approval'], 								// Write the side menu
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval'),
		[('Name') : GlobalVariable.ProspectName ], 					
		FailureHandling.STOP_ON_FAILURE)
	
	if (approvalType == 'COMSAMD' || approvalType == 'KOMISI ') {
		WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
			[('option') : 'Approve' , 								// Choose Approve or Revise
			('Remarks') : 'Automate Remarks to Revise' ],
			FailureHandling.STOP_ON_FAILURE)
	
		} else if (approvalType == 'ADJUST ') {
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
				[('option') : 'Approve' , 							// Choose Approve or Revise
				('Remarks') : 'Automate Remarks to Revise'],
				FailureHandling.STOP_ON_FAILURE)
			
		} else {
			WebUI.callTestCase(findTestCase('PAGE/OTOSALES/APPROVAL/Approval Next Page'),
				[('option') : 'Approve' , 							// Choose Approve or Reject
				('Remarks') : 'Automate Remarks to Revise' ],
				FailureHandling.STOP_ON_FAILURE)
		}
		
	WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'),
		[('menu') : 'Logout'], 								// Write the side menu
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(2)
	CustomKeywords.'general.scheduler.Otosales_Approval_Gen5'()
	WebUI.delay(2)
	CustomKeywords.'general.scheduler.Otosales_Approval_New'()
	WebUI.delay(2)
	CustomKeywords.'general.scheduler.Otosales_Approval_Next_Limit'()
	
	approvalType = UI.getValueDatabase('172.16.94.74', 'AAB', Approve, 'ApprovalType')
}

WebUI.delay(2)
CustomKeywords.'general.scheduler.Otosales_Approval_Gen5'()
WebUI.delay(2)
CustomKeywords.'general.scheduler.Otosales_Approval_New'()
WebUI.delay(2)
CustomKeywords.'general.scheduler.Otosales_Approval_Next_Limit'()
WebUI.delay(2)
CustomKeywords.'general.scheduler.Otosales_MonitorOrder'()
		


WebUI.closeBrowser()