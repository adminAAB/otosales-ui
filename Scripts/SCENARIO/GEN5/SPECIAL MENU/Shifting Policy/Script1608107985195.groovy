import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.callTestCase(findTestCase('PAGE/GEN5/LOGIN/Login'), [('username') : 'LHE', ('password') : 'P@ssw0rd', ('isSyariah') : false], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/HOME/Side Menu'), [('SideMenu') : 'Special Menu'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/SPECIAL MENU/Shift Policy Period'), [('ProcessName') : 'Shift Policy Period'
        , ('PolicyNumber') : '2039208492'], FailureHandling.STOP_ON_FAILURE)

