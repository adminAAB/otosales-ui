import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('PAGE/GEN5/CREATE URL NEW GODIG/Create New URL'),
	[('Coverage'): 'Comprehensive', 		// Comprehensive or Total Loss Only
	('Vehicle'): 'Xpander 2018 Ultimate' , 	// Fill your Vehicle Here Including year
	('Color') : 'silver' , 					// Fill Vehicle Colour
	('Plat') : 'B' , 						// List of Correct Police Number = B, F, D, L
	('PlatWrong') : 'KB' , 					// Wrong Police Number is all Police Numbers besides Correct Police Numbers
	('CekPlat') : true ,					// true if you want to check Plat validation
	('PADR') : true,						// true if you want to fill PA Driver
	('DrAmount') : '50,000,000' ,			// PA Driver Amount			
	('PAPS') : true ,						// true if you want to fill PA Passanger
	('PassAmount') : '20,000,000' ,			// PA Passanger Amount
	('TJH') : true ,						// true if you want to fill TJH
	('TJHAmount') : '25,000,000' ,			// TJH Amount
	('SRCC') : false ,						// true if you want to check SRCC
	('Terorism') : true ,					// true if you want to check Terorism
	('SurveyCity') : 'Bekasi' ,				// Fill the Survey City
	('Nama') : 'Stephen Cok' ,				// Fill the Customer's name
	('TanggalLahir') : '15/10/2002' ,		// Birth Data format is DD/MM/YYYY
	('Kelamin') : 'laki-laki' ,				// Option is laki-laki or perempuan
	('Email') : 'cok@cak.ep' ,				// Fill the Email Address
	('Phone') : '2127670262' ,				// Fill the Phone Number without "08" or "+628"
	('KTP') : '3471081510890001' ,			// Fill the Identity Number
	('Alamat') : 'Jalan Pelan-Pelan' ,		// Fill the Customer Address
	('Kodepos') : '55141' ,					// Fill the Customer Address's Post Code
	('AlamatSama') : true ,					// true if Customer Address same with Policy Delivery Address
	('AlamatPengiriman') : 'Jalan Santai' ,	// Fill the Policy Delivery Address
	('Kodepos2') : '17421' ,				// Fill the Policy Delivery Address's Post Code
	('Marketing') : 'NIF01' ],				// Fill Marketing ID >> you can find it in table mst_salesman (AAB)
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/CREATE URL NEW GODIG/Summary'),
	[('process'): 'GODIG'],		// GODIG or GEN5 Only
	FailureHandling.STOP_ON_FAILURE)





