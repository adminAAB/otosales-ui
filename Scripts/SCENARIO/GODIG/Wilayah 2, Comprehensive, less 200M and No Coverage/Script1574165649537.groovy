import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*
 Important Notes :
 1. Untuk mengubah Region, ubah pada variable Wilayah. pilihan wilayah => Wilayah 1, Wilayah 2, Wilayah 3
 2. Coverage diisi ALLRIK atau TLO
 3. List warna kendaraan : BLACK, BLUE, BROWN, COMBINATION, GREEN, GREY, NO COLOR, ORANGE, PURPLE, RED, SILVER, WHITE, YELLOW
 4. Max harga untuk PA Driver adalah 100000000 (Seratus Juta)
 5. Max harga untuk PA Passanger adalah 50000000 (Lima Puluh Juta)
 6. List Harga TJH : 5000000, 10000000, 25000000, 50000000, 100000000
 7. Lokasi survey dapat berubah mengikuti Kota Survey
 8. DeliveryAdd >> true jika ingin mencentang alamat pengiriman polis sama dengan alamat customer.
 9. List Payments :  CREDIT CARD, CREDIT CARD (INSTALLMENT 0%), BCA KlikPay, VIRTUAL ACCOUNT
 */

String Wilayah = 'Wilayah 2'

WebUI.callTestCase(findTestCase('PAGE/GODIG/Home'), 
	[('vehicleName'): 'XPANDER 2018 ULTIMATE', 
	('Region'): Wilayah ], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GODIG/Penawaran'), 
	[('Name'): 'sir cok kotelawala', 
	('Email'): 'cok@cak.ep' , 
	('Phone') : '2223334455' ], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GODIG/Step 1'), 
	[('Coverage'): 'ALLRIK', 
	('Region'): Wilayah , 
	('Color') : 'silver' , 
	('Driver') : false , 
	('Passenger') : false , 
	('TJH') : false , 
	('SRCC') : false, 
	('Terrorisme') : false , 
	('DriverAmount') : '20000000' , 
	('PassAmount') : '10000000' , 
	('TJHAmount') : '10,000,000' , 
	('SurveyCity') : 'Bekasi' ], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GODIG/Step 2'), 
	[('Kelamin'): 'laki-laki', 
	('Rumah'): 'Jalan pelan - pelan' , 
	('Kodepos') : '55141' , 
	('NoKTP') : '3471081510890001' , 
	('DeliveryAdd') : true , 
	('AlamatPengiriman') : 'Jalan Santai' , 
	('KodeposPengiriman') : '17421' ], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GODIG/Step 3'), 
	[('Payment'): 'VIRTUAL ACCOUNT'], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GODIG/Thank you'),
	[:],
	FailureHandling.STOP_ON_FAILURE)
