import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass as CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
/*
WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)
*/
// Preparation Variables
//Tambahan untuk Link Payment

String Chassis = ChassisNo
String QueryOrderNo = 'select * from ordersimulationmv WHERE chasisNumber = \''+ Chassis +'\''
String OrderNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryOrderNo, 'OrderNo' )
String QueryPolicyNo = 'select * from ordersimulation WHERE OrderNo = \''+ OrderNo +'\''
String PolicyNo = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryPolicyNo, 'PolicyNo' )


String Kendaraan = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Kendaraan'), 
    'value')
println(Kendaraan)
String Coverage = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Basic Cover'), 
    'value')

String ProductType = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Product Type'), 
    'value')

String ProductCode = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Product Code'), 
    'value')

String Usage = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Usage'), 'value')
/*
WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

boolean NeedSurvey = WebUI.verifyElementChecked(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'), GlobalVariable.DefaultDelay, FailureHandling.OPTIONAL)

// Get Premi of Extended Cover
boolean TJHAvailable = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi TJH'), 
    2, FailureHandling.OPTIONAL)

String PremiTJH = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi TJH'), FailureHandling.STOP_ON_FAILURE)
String PremiSRCC = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi SRCC'), FailureHandling.STOP_ON_FAILURE)
String PremiFlood = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Flood'), FailureHandling.STOP_ON_FAILURE)
String PremiEarth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi EarthQuake'), FailureHandling.STOP_ON_FAILURE)
String PremiDriver = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Driver'), FailureHandling.STOP_ON_FAILURE)
String PremiPassanger = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Passanger'), 
    FailureHandling.STOP_ON_FAILURE)
String PremiAccessories = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Accessories'), 
    FailureHandling.STOP_ON_FAILURE)
String GrossPremi = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text Gross Premium'), FailureHandling.STOP_ON_FAILURE)

GlobalVariable.OtosalesGrossPremi = GrossPremi

// Change Upper Case to Lower Case 
String lower = CustUsage.toLowerCase()
String upper = lower.replace(lower[0], lower[0].toUpperCase())

// Check if the images are present
boolean ImageKTP = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image', 
        [('image') : 'KTP/KITAS']), 1, FailureHandling.OPTIONAL)

boolean ImageSTNK = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image', 
        [('image') : 'STNK']), 1, FailureHandling.OPTIONAL)

boolean ImageBSTB = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image', 
        [('image') : 'BSTB']), 1, FailureHandling.OPTIONAL)

boolean ImageKonf = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image', 
        [('image') : 'KONFIRMASI']), 1, FailureHandling.OPTIONAL)

boolean ImageSPPAKB = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image',
	[('image') : 'SPPAKB']), 1, FailureHandling.OPTIONAL)

boolean ImageNPWP = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image',
	[('image') : 'NPWP']), 1, FailureHandling.OPTIONAL)

boolean ImageSIUP = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Check Image',
	[('image') : 'SIUP']), 1, FailureHandling.OPTIONAL)

boolean Personal = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Customer Name'), 2, FailureHandling.OPTIONAL)
boolean Company = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Company Name'), 2, FailureHandling.OPTIONAL)

// Action Script
if (Personal == true) {
	String CustName = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Customer Name'), 'value', FailureHandling.OPTIONAL)
	
	String CustBirth = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Customer Birthday'),
		'value')
	
	if (CustName == ProspectName) {
	    println('Customer Name is correct')
	} else {
	    println('Error : Customer Name is incorrect')
	}

	if (CustBirth != 'No Data') {
		KeywordUtil.markPassed('Customer Birthday has been selected')

	} else {
	    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Customer Birthday'), FailureHandling.STOP_ON_FAILURE)
		CustomKeywords.'general.web.reactDatePicker'(birthDate)
	}
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Jenis Kelamin'))
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Jenis Kelamin - Option', [('sex') : JenisKelamin]))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text field Alamat'), Alamat, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Kodepos Customer'))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Field Search Kode Pos'), KodePos, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Kode Pos', [('postal') : KodePos]))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text field Personal HP'))
	
	String CustHp = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text field Personal HP'),
		'value')
	
	if ((CustHp == CustPhone) && (CustHp != '')) {
		KeywordUtil.markPassed('Customer Phone Number is correct')
		
	} else if (CustHp == '') {
		KeywordUtil.markWarning('WARNING : Customer Phone Number is Empty')
	    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text field Personal HP'), CustPhone, FailureHandling.STOP_ON_FAILURE)
	
	} else {
		KeywordUtil.markWarning('Customer Phone Number is masked to (' + CustHp + ')')
	}
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text Field Nomor ID'), Identitas, FailureHandling.STOP_ON_FAILURE)
	
	if (docRep) {
	    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Doc Rep'))
	
	    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text Field Doc Rep'), DocRepAmount, FailureHandling.STOP_ON_FAILURE)
	
	    if (uploadDocRep) {
	        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'DOC REP']))
	
	        WebUI.delay(1)
	
	        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
	        WebUI.delay(5)
			
			UI.UploadFile2 ('jackie chan.jpg')
			
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	    }
	}
	
	if (!(ImageKTP)) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'KTP/KITAS']))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
		WebUI.delay(5)
		
		UI.UploadFile2 ('sir john kotelawala.jpeg')
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
	
	if (!(ImageSTNK)) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'STNK']))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
		WebUI.delay(5)
		
		UI.UploadFile2('stephen chow.jpeg')
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
	
	if (!(ImageBSTB)) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'BSTB']))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
		WebUI.delay(5)
	
		UI.UploadFile2('Nicolas Cage.jpeg')
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
	
} else if (Company == true) {
	String CompName = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Company Name'),'value')
	String NPWPDate = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Tanggal NPWP'),'value')
	
	if (CompName == ProspectName) {
		KeywordUtil.markPassed('Company Name is correct')

	} else {
		KeywordUtil.markFailedAndStop('FAILED : Company Name is incorrect')

	}
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Nomor NPWP'), NomorNPWP, FailureHandling.STOP_ON_FAILURE)
	
	if (NPWPDate != 'No Data') {
		KeywordUtil.markWarning('NPWP Date has been selected')

	} else {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Tanggal NPWP'), FailureHandling.STOP_ON_FAILURE)
		
		CustomKeywords.'general.web.reactDatePicker'(birthDate)
	}
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Alamat NPWP'), Alamat, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Kodepos Customer'))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Field Search Kode Pos'), KodePos, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Company Postal Code', [('postal') : '55141']))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input PIC Company'),PICName , FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input PIC Hp'))
	
	WebUI.delay(1)
	
	String PICHp = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input PIC Hp'),
		'value')
	
	if ((PICHp == CustPhone) && (PICHp != '')) {
		KeywordUtil.markPassed('Customer Phone Number is correct')

	} else if (PICHp == '') {
		KeywordUtil.markWarning('WARNING: Customer Phone Number is Empty')
	
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Text field Personal HP'), CustPhone, FailureHandling.STOP_ON_FAILURE)
	} else {
		KeywordUtil.markWarning('Customer Phone Number is masked to (' + PICHp + ')')
	}
	
	if (!(ImageNPWP)) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'NPWP']))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
		WebUI.delay(5)
		
		UI.UploadFile2('sir john kotelawala.jpeg')	
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
	
	if (!(ImageSIUP)) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'SIUP']))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
		WebUI.delay(5)
		
		UI.UploadFile2('stephen chow.jpeg')	
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}

if (!(ImageKonf)) {
    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'KONFIRMASI']))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))

    WebUI.delay(5)
	
	UI.UploadFile2('jackie chan.jpg')
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
}

if (Company == true) {
	if (Paid) {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Bukti Bayar'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
		
		WebUI.delay(5)
		UI.UploadFile2('jackie chan.jpg')
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}

if (payerCompany) {
    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Payer Company'))
}
*/
WebUI.delay(1)

if (Kendaraan != CustVehicle) {
    KeywordUtil.markFailed('FAILED - Vehicle is not Match with Input Prospect / Premium Simulation')

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Kendaraan'))

    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Search Kendaraan'), CustVehicle)

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Kendaraan', [('key1') : CustVehicle]))
} else {
    KeywordUtil.markPassed('PASSED - Vehicle is Correct')
}

//Pengecekan Link Payment
if (PolicyNo != '') {
	println(ProductType)
	if (ProductType == 'Garda Oto Syariah'){
		KeywordUtil.markPassed('PASSED - Product Type Garda Oto Syariah tidak muncul Link Payment')
	
		
	}
	else {
	boolean CheckboxFullPayment = WebUI.verifyElementChecked(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox CC Full Payment'), 2, FailureHandling.OPTIONAL)
	boolean CheckboxInstallment = WebUI.verifyElementChecked(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox CC Installment'), 2, FailureHandling.OPTIONAL)
	String LinkPayment = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Textarea Link Payment'),'value')
	boolean FullPayment = CreditCardFullPayment
	println(CheckboxFullPayment)
	println(CheckboxInstallment)
	println(LinkPayment)
	if (CheckboxFullPayment){
		if (LinkPayment == ''){
			if (FullPayment == true) {
				
				WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Button Generate Link Payment'))
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(10)
				String LinkPaymentText = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Textarea Link Payment'),'value')
				println(LinkPaymentText)
				if (LinkPaymentText != ''){
					KeywordUtil.markPassed('PASSED - Link Payment berhasil tergenerate')
				}
				else {
					KeywordUtil.markFailedAndStop('FAILED - Link Payment gagal tergenerate')
				}
			}
			else {
		
				WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox CC Installment'))
				WebUI.delay(5)
				WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Button Generate Link Payment'))
				WebUI.delay(5)
				boolean NotifAppLinkPayment = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi TJH'),
						2, FailureHandling.OPTIONAL)
				String QueryAABHead = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ GlobalVariable.OtosalesLogin +'\''
				String TypeLogin = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABHead, 'Type')
				
				if(TypeLogin == 'TELE'){
					String QueryAABHeadTELE = 'SELECT * FROM dbo.SalesOfficer WHERE Role = \'TELEMGR\''
					String Login = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABHeadTELE, 'Value')
					String Notifikasi = 'Request Pembayaran dengan cicilan sedang diproses. Link payment akan muncul setelah mendapatkan persetujuan '+Login+'.'
					String CekNotif = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Pop up Approval Link Payment'),'value')
					
					println(Login)
					println(Notifikasi)
					println(CekNotif)
					if (NotifAppLinkPayment) {
						if(CekNotif == Notifikasi){
							
							KeywordUtil.markPassed('PASSED - Notifikasi Link Payment yang muncul sesuai')
							WebUI.delay(5)
						}
						else{
							KeywordUtil.markFailedAndStop('FAILED - Notifikasi Link Payment yang muncul tidak sesuai')
						}
					}
				}
				else {
					String QueryAABHeadSO = 'SELECT TOP 1* FROM dbo.AABHead WHERE param = \''+ GlobalVariable.OtosalesLogin +'\''
					String Login = UI.getValueDatabase('172.16.94.48', 'AABMobile', QueryAABHeadSO, 'Value')
					String Notifikasi = 'Request Pembayaran dengan cicilan sedang diproses. Link payment akan muncul setelah mendapatkan persetujuan '+Login+'.'
					String CekNotif = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Pop up Approval Link Payment'),'value')
					
					println(Login)
					println(Notifikasi)
					println(CekNotif)
					if (NotifAppLinkPayment) {
						if(CekNotif == Notifikasi){
							
							KeywordUtil.markPassed('PASSED - Notifikasi Link Payment yang muncul sesuai')
							WebUI.delay(5)
						}
						else{
							KeywordUtil.markFailedAndStop('FAILED - Notifikasi Link Payment yang muncul tidak sesuai')
						}
					}
					
				}
				    
				

			}
		}
		else{
			KeywordUtil.markFailedAndStop('FAILED - Text Area Link Payment is not Empty')
		}
	}
	else {
		KeywordUtil.markFailedAndStop('FAILED - Checklist Credit Card FUll Payment is not Default')
	}	
}

}

/*
WebUI.delay(1)

if (Coverage != Coverage) {
    KeywordUtil.markFailed('FAILED - Coverage is not match with Input Prospect / Premium Simulation')

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Basic Cover'))

    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Search Coverage'), Coverage)

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Coverage', [('cover') : Coverage]))
} else {
    KeywordUtil.markPassed('PASSED - Coverage is Correct')
}

if (changePeriod) {
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Open Period From'), GlobalVariable.LongestDelay)
	
	// Ubah Period From
    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Open Period From'))

    CustomKeywords.'general.web.reactDatePicker'(periodFrom)
	
	// Ubah Period To
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Open Period To'))
	
	CustomKeywords.'general.web.reactDatePicker'(periodTo)
}

WebUI.delay(1)

if (Usage != upper) {
    KeywordUtil.markFailed('FAILED - Usage is not match with Input Prospect / Premium Simulation')

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Usage'))

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Usage', [('key2') : upper]))
} else {
    KeywordUtil.markPassed('PASSED - Usage is Correct')
}

WebUI.delay(1)
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Vehicle Color'), WarnaKendaraan)
WebUI.delay(1)

if (ProductType != GlobalVariable.ProductType) {
    KeywordUtil.markFailed('FAILED - Product Type is not match with Input Prospect / Premium Simulation')

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Product Type'))

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type', [('key1') : GlobalVariable.ProductType]))
} else {
    KeywordUtil.markPassed('PASSED - Product Type is Correct')
}

WebUI.delay(1)
//vehicle

if (ProductCode != GlobalVariable.ProductCode) {
    KeywordUtil.markFailed('FAILED - Product Code is not match with Input Prospect / Premium Simulation')

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Product Code'))

    WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Code', [('key2') : GlobalVariable.ProductCode]))
} else {
    KeywordUtil.markPassed('PASSED - Product Code is Correct')
}

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Segment Code'))

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Search Segment Code'), SegmentCode)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Segment Code', [('segment') : SegmentCode]))

WebUI.delay(1)

String PlateNo = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Plate No'), 'value')
String ChasisNo = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Chassis No'),
	'value')
String EngineNo = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Engine No'),
	'value')

if (PlateNo != GlobalVariable.RegistrationNo) {
    KeywordUtil.markFailed('FAILED - Registration No is not match with Input Prospect / Premium Simulation')

    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Plate No'), GlobalVariable.RegistrationNo)
} else {
    KeywordUtil.markPassed('PASSED - Registration No is Correct')
}

WebUI.delay(1)

if (ChasisNo != GlobalVariable.ChassisNo) {
    KeywordUtil.markFailed('FAILED - Chasis No is not match with Input Prospect / Premium Simulation')

    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Chassis No'), GlobalVariable.ChassisNo)
} else {
    KeywordUtil.markPassed('PASSED - Chasis No is Correct')
}

WebUI.delay(1)

if (EngineNo != GlobalVariable.EngineNo) {
    KeywordUtil.markFailed('FAILED - Engine No is not match with Input Prospect / Premium Simulation')

    WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Engine No'), GlobalVariable.EngineNo)
} else {
    KeywordUtil.markPassed('PASSED - Engine No is Correct')
}

WebUI.delay(1)

if (TJHAvailable) {
    if (PremiTJH == GlobalVariable.PremiTJH) {
        KeywordUtil.markPassed('PASSED - TJH Premium is correct')
    } else {
        KeywordUtil.markFailed('FAILED - TJH Premium is different with Input Prospect / Premium Simulation')
    }
} else {
    KeywordUtil.markWarning('Field TJH is not displayed')
}

WebUI.delay(1)

if (PremiSRCC == GlobalVariable.PremiSRCC) {
    KeywordUtil.markPassed('PASSED - SRCC Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - SRCC Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)

if (PremiFlood == GlobalVariable.PremiFlood) {
    KeywordUtil.markPassed('PASSED - Flood Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - Flood Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)

if (PremiEarth == GlobalVariable.PremiEarthquake) {
    KeywordUtil.markPassed('PASSED - Earthquake Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - Earthquake Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)
if (Terrorism) {
	
	String PremiTeror = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Terrorism'), FailureHandling.STOP_ON_FAILURE)

	if (PremiTeror == GlobalVariable.PremiTerrorism) {
	    KeywordUtil.markPassed('PASSED - Terrorism Premium is correct')
	} else {
	    KeywordUtil.markWarning('FAILED - Terrorism Premium is different with Input Prospect / Premium Simulation')
	}
}
WebUI.delay(1)

if (PremiDriver == GlobalVariable.PremiDriver) {
    KeywordUtil.markPassed('PASSED - PA Driver Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - PA Driver Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)

if (PremiPassanger == GlobalVariable.PremiPassanger) {
    KeywordUtil.markPassed('PASSED - Passanger Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - Passanger Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)

if (PremiAccessories == GlobalVariable.PremiAccessories) {
    KeywordUtil.markPassed('PASSED - Accessories Premium is correct')
} else {
    KeywordUtil.markFailed('FAILED - Accessories Premium is different with Input Prospect / Premium Simulation')
}

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox dikirim ke'))

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox dikirim ke - option', [('sent') : SentTo]))

WebUI.delay(1)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

switch (SentTo) {
    case 'Branch':
        break
    case 'Customer':
        break
    case 'Garda Center':
        WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Nama Pengiriman'), 'Nama Bebas', 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Alamat Pengiriman'))

        WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Search Alamat Pengiriman'), AlamatGC, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Alamat Pengiriman', [('alamat') : AlamatGC]))
		break
	case 'Others' :
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Others name'), 'Nama Bebas', FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Others Address'), 'Alamat Bebas', FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Others Kodepos'))
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Search Others KodePos'), KodePosOther, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Others KodePos', [('other') : KodePosOther ]), FailureHandling.STOP_ON_FAILURE)
		break
}

WebUI.delay(2)

if (NeedSurvey) {
	if (NeedNSA) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox NSA Skip Survey'), GlobalVariable.LongestDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox NSA Skip Survey'))
		
	} else if (SurveyAdd) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Additional Survey'), GlobalVariable.LongestDelay)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Additional Survey'))
		
	} else if (!survey) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'), GlobalVariable.LongestDelay)
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'))
		
	} else {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Kota Survey'), GlobalVariable.LongestDelay)	
	
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Kota Survey'))
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Search Kota Survey'), kotaSurvey, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox dikirim ke - option', [('sent') : kotaSurvey]))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Lokasi Survey'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Date Survey'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Combobox Waktu Survey'))
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Choose Waktu Survey'))
	}
} else if (NeedSurvey == false && survey == true) {
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'), GlobalVariable.LongestDelay)
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Checkbox Need Survey'))
}

if (!ImageSPPAKB) {
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'SPPAKB']))

	WebUI.delay(1)

	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))

	WebUI.delay(5)
	 
	UI.UploadFile2('stephen chow.jpeg')
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
}

if (NSAApproval) {
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Upload Image', [('image') : 'NSA']))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Choose Image'))
	
	WebUI.delay(5)
	
	UI.UploadFile2('Nicolas Cage.jpeg')
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
}

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Input Remarks to SA'), RemarkToSA, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Footer Menu', [('menu') : Status ]))

WebUI.delay(2)

//WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)
*/
