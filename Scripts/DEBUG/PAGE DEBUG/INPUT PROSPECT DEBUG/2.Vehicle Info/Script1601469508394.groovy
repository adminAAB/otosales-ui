import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Wait Entire Page displayed
WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

// Write Plate, Chassis dan Engine based on Respected Region

String RegistrationNo = UI.getValueDatabase("172.16.94.48", "LiTT", 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\'', "Value")
String ChassisNo = UI.getValueDatabase("172.16.94.48", "LiTT", 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Chassis Number\'', "Value")
String EngineNo = UI.getValueDatabase("172.16.94.48", "LiTT", 'SELECT * FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Engine Number\'', "Value")

String UpperVehicle = VehicleName.toUpperCase()

def Plate = CustomKeywords.'general.web.addRegistration'(Region, RegistrationNo)
def Rangka = CustomKeywords.'general.web.addChassis'(Region, ChassisNo)
def Mesin = CustomKeywords.'general.web.addEngine'(Region, EngineNo)

GlobalVariable.RegistrationNo = Plate
GlobalVariable.ChassisNo = Rangka
GlobalVariable.EngineNo = Mesin

// Action Begin
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Kendaraan'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Search Kendaraan'), UpperVehicle)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Kendaraan',[('key1') : UpperVehicle ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Kendaraan',[('key1') : UpperVehicle ]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Usage'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT/Search Usage'), Usage)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Usage',[('key2') : Usage ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Usage',[('key2') : Usage ]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Region'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Region',[('key3') : Region ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Choose Region',[('key3') : Region ]))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Registration No'), Plate)
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Chassis No'), Rangka)
WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Engine No'), Mesin)

// Update Value in database
UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Registration Number\'')
UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Chassis Number\'')
UI.updateValueDatabase('172.16.94.48', 'LiTT', 'UPDATE [LiTT].[dbo].[Otosales] SET Value += 1 WHERE Parameters = \'Engine Number\'')

// Store Data
String Vehicle = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Kendaraan'), 'value')
String Usage = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Usage'), 'value')
String Region = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Input Region'), 'value')

GlobalVariable.CustVehicle = Vehicle
GlobalVariable.CustUsage = Usage
GlobalVariable.CustRegion = Region

// Press Next Button
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Button Next'))




