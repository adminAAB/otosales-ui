//<<<<<<< HEAD
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

// Variables
String[] separate = Coverage.split(" ")

int parseInt = CustomKeywords.'general.web.getInteger'(TJH)
int parseIntDR = CustomKeywords.'general.web.getInteger'(Driver)
int parseIntPas = CustomKeywords.'general.web.getInteger'(Passenger)
int parseAcc = Accessory.toInteger()

String attributeTSI = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TSI'), 'value')

int attribute = CustomKeywords.'general.web.getInteger'(attributeTSI)
// Start Action
WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Type'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]))	

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

//WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]), 5)

//WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Code'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Search Product Code'), ProductCode)

//HRM
//WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Code',[('key2') : ProductCode ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

// tambah button search 
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button Search Product'))
// end
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Code',[('key2') : ProductCode ]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Basic Cover'))

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Search Basic Cover'), Coverage)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Basic Cover',[('key3') : Coverage ]), GlobalVariable.MediumDelay)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Basic Cover',[('key3') : Coverage ]))

WebUI.delay(1)

//Set TJH
boolean CheckTJH = WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.LongestDelay, FailureHandling.OPTIONAL)

if (!CheckTJH && separate[0] != 'TLO' && ProductType != 'Lexus Insurance') {
	KeywordUtil.markFailedAndStop("ERROR - Checkbox TJH is not Displayed")
} else {
	if(ProductType != 'Lexus Insurance'){
		if (separate[0] != 'TLO') {
			if (parseInt > 0){
				WebUI.delay(1)
				
//				WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.MediumDelay)
				
				WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.MediumDelay)
				
				WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'))
	
				//Jika product IEP
				//|| (CashDealer == true && ProductType == 'Garda Oto')
				if((ProductCode == 'GNAB0') ){
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'))
					
					WebUI.delay(1)
					
					WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'), TJH)
					
					WebUI.delay(1)
					
					WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'), 10)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'))
					
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
			   
				 } else {
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
						
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'))
					
					WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TJH', [('key4') : TJH ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TJH', [('key4') : TJH ]))
					
					WebUI.delay(1)
					
					WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'), 10)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'))
					
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
				}
			}
		} else {
			println('Error - TJH must not displayed')
		}
	}
	else {
		println('Product Lexus Insurance do not have TJH')
	}
}

// Set SRCC - Flood - EarthQuake
 if (separate[0] == 'Comprehensive' && attribute < 200000000) {
	WebUI.delay(1)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'), GlobalVariable.MediumDelay)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'))
	
	WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
} else if (ProductCode == 'GNAB0') {
	WebUI.delay(1)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'), GlobalVariable.MediumDelay)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'))
	
	WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
} 
else {
	def disable = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC - Disabled'), 10)

	if (disable && separate[0] == 'Comprehensive' && attribute > 200000000){
		println('Coverage is Comprehensive and TSI Above 200,000,000, therefor this menu is disable')
		
	} else if (disable && separate[0] != 'Comprehensive') {
		println('Coverage is TLO, therefor this menu is disable')
	
	} else if (!disable){
		println ('Error - This checkbox should be disable')
		
	} else {
		println('This checkbox disable because coverage contains TLO')
	}
}

// Set Terrorism
if (Terrorism) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Terrorisme'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Terrorisme'))
		
		WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		String PremiTeror = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Terrorism'), FailureHandling.STOP_ON_FAILURE)
		GlobalVariable.PremiTerrorism = PremiTeror
	}	
	
}
	
// Set PA Driver 
if (parseIntDR > 0) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Driver'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Driver', [('key5') : Driver ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Driver', [('key5') : Driver ]))
		
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'), 10)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	} 
	else {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Driver'), Driver)
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}
else {
		println('PA Driver is not set')
	}

// Set PA Passenger
if (parseIntPas > 0) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Passenger'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Passenger', [('key6') : Passenger ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Passenger', [('key6') : Passenger ]))
		
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'), 10)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	} else {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Passenger'), Passenger)
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
} else {
	println('PA Passenger is not set')
}

// Set Accessory
/*GlobalVariable.OrderType = MV*/

if (MV == 'MV NON GODIG') {
	if (parseAcc > 0) {
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Accessory'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Accessory'))

		WebUI.delay(1)
		
		WebUI.clearText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Accessory'), FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Accessory'), Accessory)
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}

// Store Data
	String ProductType = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Type'), 'value')
	String ProductCode = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Code'), 'value')
	String BasicCover = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Basic Cover'), 'value')
	String PremiTJH = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi TJH'), FailureHandling.STOP_ON_FAILURE)
	String PremiSRCC = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi SRCC'), FailureHandling.STOP_ON_FAILURE)
	String PremiFlood = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Flood'), FailureHandling.STOP_ON_FAILURE)
	String PremiEarth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi EarthQuake'), FailureHandling.STOP_ON_FAILURE)
	//String PremiTeror = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Terrorism'), FailureHandling.STOP_ON_FAILURE)
	String PremiDriver = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Driver'), FailureHandling.STOP_ON_FAILURE)
	String PremiPassanger = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Passanger'), FailureHandling.STOP_ON_FAILURE)
	String PremiAccessories = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Accessories'), FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.ProductType = ProductType
	GlobalVariable.ProductCode = ProductCode
	GlobalVariable.Coverage = BasicCover
	GlobalVariable.PremiTJH = PremiTJH
	GlobalVariable.PremiSRCC = PremiSRCC
	GlobalVariable.PremiFlood = PremiFlood
	GlobalVariable.PremiEarthquake = PremiEarth
	//GlobalVariable.PremiTerrorism = PremiTeror
	GlobalVariable.PremiDriver = PremiDriver
	GlobalVariable.PremiPassanger = PremiPassanger
	GlobalVariable.PremiAccessories = PremiAccessories
	
// Press Button Next
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Button Next'))
/*
//=======
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

// Variables
String[] separate = Coverage.split(" ")

int parseInt = CustomKeywords.'general.web.getInteger'(TJH)
int parseIntDR = CustomKeywords.'general.web.getInteger'(Driver)
int parseIntPas = CustomKeywords.'general.web.getInteger'(Passenger)
int parseAcc = Accessory.toInteger()

String attribute = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TSI'), 'value')

// Start Action
WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Type'))

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]))	

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

//WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Type',[('key1') : ProductType ]), 5)

//WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Code'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Search Product Code'), ProductCode)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Code',[('key2') : ProductCode ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Product Code',[('key2') : ProductCode ]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Basic Cover'))

WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Search Basic Cover'), Coverage)

WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Basic Cover',[('key3') : Coverage ]), GlobalVariable.MediumDelay)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Choose Basic Cover',[('key3') : Coverage ]))

WebUI.delay(1)

//Set TJH
boolean CheckTJH = WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.LongestDelay, FailureHandling.OPTIONAL)

if (!CheckTJH && separate[0] != 'TLO' && ProductType != 'Lexus Insurance') {
	KeywordUtil.markFailedAndStop("ERROR - Checkbox TJH is not Displayed")
} else {
	if(ProductType != 'Lexus Insurance'){
		if (separate[0] != 'TLO') {
			if (parseInt > 0){
				WebUI.delay(1)
				
//				WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.MediumDelay)
				
				WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'), GlobalVariable.MediumDelay)
				
				WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox TJH'))
	
				//Jika product IEP
				if((ProductCode == 'GNAB0') || (CashDealer == true && ProductType == 'Garda Oto')){
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'))
					
					WebUI.delay(1)
					
					WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'), TJH)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'))
					
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
			   
				 } else {
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
						
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open TJH'))
					
					WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TJH', [('key4') : TJH ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input TJH', [('key4') : TJH ]))
					
					WebUI.delay(1)
					
					WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'), 10)
					
					WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK TJH'))
					
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
				}
			}
		} else {
			println('Error - TJH must not displayed')
		}
	}
	else {
		println('Product Lexus Insurance do not have TJH')
	}
}

// Set SRCC - Flood - EarthQuake
if (separate[0] == 'Comprehensive' && attribute < '200,000,000') {
	WebUI.delay(1)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'), GlobalVariable.MediumDelay)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'))
	
	WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
} else if (ProductCode == 'GNAB0') {
	WebUI.delay(1)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'), GlobalVariable.MediumDelay)
	
	WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC'))
	
	WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	
} else {
	def disable = WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox SRCC - Disabled'), 10)

	if (disable && separate[0] == 'Comprehensive' && attribute > '200,000,000'){
		println('Coverage is Comprehensive and TSI Above 200,000,000, therefor this menu is disable')
		
	} else if (disable && separate[0] != 'Comprehensive') {
		println('Coverage is TLO, therefor this menu is disable')
	
	} else if (!disable){
		println ('Error - This checkbox should be disable')
		
	} else {
		println('This checkbox disable because coverage contains TLO')
	}
}

// Set Terrorism
if (Terrorism) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Terrorisme'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Terrorisme'))
		
		WebUI.click(findTestObject('OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}	
}
	
// Set PA Driver 
if (parseIntDR > 0) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Driver'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Driver', [('key5') : Driver ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Driver', [('key5') : Driver ]))
		
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'), 10)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	} 
	else {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Driver'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Driver'), Driver)
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}
else {
		println('PA Driver is not set')
	}

// Set PA Passenger
if (parseIntPas > 0) {
	if(ProductType != 'Lexus Insurance'){
		WebUI.delay(1)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Passenger'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Passenger', [('key6') : Passenger ]), GlobalVariable.MediumDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input PA Passenger', [('key6') : Passenger ]))
		
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'), 10)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	} else {
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox PA Passenger'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Open PA Passenger'), Passenger)
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
} else {
	println('PA Passenger is not set')
}

// Set Accessory
/*GlobalVariable.OrderType = MV*/
/*
if (MV == 'MV NON GODIG') {
	if (parseAcc > 0) {
		WebUI.delay(2)
		
		WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Accessory'), GlobalVariable.MediumDelay)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Checkbox Accessory'))

		WebUI.delay(1)
		
		WebUI.clearText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Accessory'), FailureHandling.STOP_ON_FAILURE)
		
		WebUI.setText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Accessory'), Accessory)
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Button OK Pop up'))
		
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
	}
}

// Store Data
	String ProductType = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Type'), 'value')
	String ProductCode = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Product Code'), 'value')
	String BasicCover = WebUI.getAttribute(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Input Basic Cover'), 'value')
	String PremiTJH = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi TJH'), FailureHandling.STOP_ON_FAILURE)
	String PremiSRCC = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi SRCC'), FailureHandling.STOP_ON_FAILURE)
	String PremiFlood = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Flood'), FailureHandling.STOP_ON_FAILURE)
	String PremiEarth = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi EarthQuake'), FailureHandling.STOP_ON_FAILURE)
	String PremiTeror = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Terrorism'), FailureHandling.STOP_ON_FAILURE)
	String PremiDriver = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Driver'), FailureHandling.STOP_ON_FAILURE)
	String PremiPassanger = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi PA Passanger'), FailureHandling.STOP_ON_FAILURE)
	String PremiAccessories = WebUI.getText(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 3/Premi Accessories'), FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.ProductType = ProductType
	GlobalVariable.ProductCode = ProductCode
	GlobalVariable.Coverage = BasicCover
	GlobalVariable.PremiTJH = PremiTJH
	GlobalVariable.PremiSRCC = PremiSRCC
	GlobalVariable.PremiFlood = PremiFlood
	GlobalVariable.PremiEarthquake = PremiEarth
	GlobalVariable.PremiTerrorism = PremiTeror
	GlobalVariable.PremiDriver = PremiDriver
	GlobalVariable.PremiPassanger = PremiPassanger
	GlobalVariable.PremiAccessories = PremiAccessories
	
// Press Button Next
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OTOSALES/NEW/INPUT PROSPECT 2/Button Next'))

//>>>>>>> master

*/

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)


