import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('DEBUG/Login AO DEBUG'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/Side Menu-Switch DEBUG'), [('menu') : 'Input Prospect'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/PAGE DEBUG/INPUT PROSPECT DEBUG/1.Prospect Info'), [('MV') : 'MV NON GODIG', ('isCompany') : false
        , ('phone1') : '08987878788', ('prospectName') : 'Dinda'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/PAGE DEBUG/INPUT PROSPECT DEBUG/2.Vehicle Info'), [('Usage') : 'PRIBADI', ('Region') : 'Wilayah 2'
        , ('VehicleName') : 'TOYOTA ALPHARD 2018 '], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/PAGE DEBUG/INPUT PROSPECT DEBUG/3.Cover'), [('Coverage') : 'Comprehensive 1 Year', ('ProductCode') : 'GWU50'
        , ('ProductType') : 'Garda Oto', ('Passenger') : '10,000,000', ('Driver') : '20,000,000', ('Terrorism') : true, ('TJH') : '25,000,000'
        , ('Accessory') : '7000000', ('CashDealer') : false, ('MV') : GlobalVariable.OrderType], FailureHandling.STOP_ON_FAILURE)

