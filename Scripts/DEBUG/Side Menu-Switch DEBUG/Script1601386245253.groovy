import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/OTOSALES/LOGIN/Otosales Entire Page'), 3)

WebUI.waitForElementClickable(findTestObject('Object Repository/OTOSALES/LOGIN/Otosales Entire Page'), 3)

switch (menu){
	case 'Task List' :
	
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Tasklist'))
	
		break
		
	case 'Input Prospect' :
	
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Input Prospect'))
	
		break
		
	case 'Premium Simulation' :
		
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Premium Simulation'))
		
		break
		
	case 'Order Approval' :
		
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Order Approval'))
		
		break
		
	case 'Dashboard' :
		
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Dashboard'))
		
		break
		
	case 'Logout' :
		
		WebUI.click(findTestObject('OTOSALES/Side Menu/Side Menu - Logout'))
		
		break
}

WebUI.waitForElementNotPresent(findTestObject('Object Repository/OTOSALES/NEW/TASKLIST/Details/Loading'), GlobalVariable.LongestDelay, FailureHandling.STOP_ON_FAILURE)
