import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('DEBUG/Login AO DEBUG'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/Side Menu-Switch DEBUG'), [('menu') : 'Task List'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/PAGE DEBUG/TASKLIST DEBUG/1.Home'), [('CustName') : 'AUTOMATE CUSTOMER NON GODIG 2350'
        , ('Chassis') : 'B2350ATM'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('DEBUG/PAGE DEBUG/TASKLIST DEBUG/2.Tasklist'), [('CustUsage') : 'PRIBADI', ('CustVehicle') : 'MERCEDES BENZ C-CLASS 2018 C 200 EDITION C A/T'
        , ('Coverage') : 'Comprehensive 1 Year', ('ProductType') : 'Garda Oto', ('ProductCode') : 'GDU52', ('RegistrationNo') : 'B2350ATM'
        , ('ChassisNo') : 'B2350ATM', ('EngineNo') : 'B2350ATM', ('ProspectName') : 'AUTOMATE CUSTOMER NON GODIG 2350', ('CustPhone') : '082127670262'
        , ('CreditCardFullPayment') : true], FailureHandling.STOP_ON_FAILURE)

